\Chapter{PRÉLIMINAIRES}\label{sec:Prelim}

Dans ce chapitre, nous introduisons les définitions usuelles de modélisation de systèmes, afin de construire les fondations du formalisme choisi par la suite. Nous introduisons tout d'abord les notions générales de modélisation de systèmes probabilistes, puis nous présentons une propriété de sécurité particulière, l'opacité.


%%
%% I - Spécification
%%
\section{Modélisation}\label{spec}
L'étape préliminaire à toute vérification formelle d'un système sécurisé est sa modélisation. Cette partie a pour but de définir les modèles usuels qui constituent les bases du cadre de notre étude.

% langages $\omega$-réguliers et automates (Büchi, DPA)
\subsection{Langages et automates}
% Un système se caractérise tout d'abord par un ensemble de propriétés dites \emph{atomiques}, qu'il peut vérifier ou non suivant l'état de son exécution. On suppose que ces propriétés sont en nombre fini.

% Si $a$ et $b$ sont les deux seules propriétés atomiques d'un système, il est à noter que le système, à tout instant, peut vérifier soit uniquement $a$, soit uniquement $b$, soit $a$ et $b$ simultanément, soit ni $a$ ni $b$. On peut alors désigner l'état des propriétés atomiques du système comme un nombre binaire de longueur 2. Par exemple, si le système est dans l'état $(01)$, cela signifie qu'il ne vérifie pas $a$ mais qu'il vérifie $b$ à l'instant considéré. Ainsi, on note $\Sigma$ l'\emph{alphabet} contenant l'ensemble des états des propriétés atomiques du système. Dans l'exemple ci-dessus, $\Sigma=\{00,01,10,11\}$. 

% Dans l'éventualité où les propriétés atomiques sont deux-à-deux exclusives -- c'est-à-dire que le système ne vérifie qu'une seule propriété atomique à la fois -- on peut considérer l'alphabet comme étant l'ensemble des propriétés atomiques. Dans l'exemple ci-dessus, si $a$ et $b$ sont mutuellement exclusives, alors $\Sigma=\{a,b\}$.

À partir d'un alphabet $\Sigma$, on peut construire des mots de longueur finie quelconque ou bien des mots de longueur infinie. On appelle \emph{langage} un ensemble de mots issus d'un alphabet. On note $\Sigma^k$ le langage contenant tous les mots de longueur $k$. On note $\Sigma^*$ le langage contenant tous les mots de longueur finie -- formellement, $\Sigma^*=\bigcup_{k\in\mathbb{N}} \Sigma^k$; le langage contenant tous les mots de longueur infinie est noté $\Sigma\om$. L'union de ces deux langages est noté $\Sigma^{\infty}=\Sigma\om\cup\Sigma^*$.

Un premier outil mathématique lié à la théorie des langages est la notion d'automate \citep[][]{main,Piterman07}. 

\begin{mydef}[Automate]\label{def:automateBuchi}
Un automate est un tuple \[\mathcal{A}=(Q,\Sigma,\delta,q_0,Q_f)\] tel que
\begin{itemize}
\item $Q$ est un ensemble fini d'états, dont $q_0$ qui est l'état initial ;
\item $\Sigma$ est un alphabet ;
\item $\delta~: Q\times\Sigma \rightarrow Q$ est une fonction de transition ; 
\item $Q_f$ est un sous-ensemble de $Q$, composé des états dits acceptants.
\end{itemize}
\end{mydef}

C'est une machine à états finis dont le but est de reconnaître si un mot appartient ou non à un langage donné, lié à l'automate. Pour cela, la machine lit le mot caractère par caractère. Chaque fois qu'elle lit un caractère, elle change d'état en concordance avec sa fonction de transition. Cette procédure s'appelle l'\emph{exécution} du mot par l'automate. Dans le cas d'un mot fini, on regarde dans quel état se trouve l'automate à la fin de la lecture~: le mot appartient au langage si, et seulement si, l'automate à la fin de l'exécution est dans un état acceptant.

Cette méthode doit cependant être modifiée dans le cadre des mots infinis. Pour cela, l'automate généralement utilisé est l'automate de Büchi. La définition d'un automate de Büchi est exactement la définition \ref{def:automateBuchi}, la différence étant dans la sémantique de ce modèle. Dans un automate de Büchi, un mot est reconnu si, et seulement si, durant sa lecture, l'automate passe infiniment souvent par des états acceptants. Ainsi, les automates de Büchi sont des extensions directes des automates sur les mots finis. Cependant, alors que dans le cas fini, les automates non-déterministes se déterminisent sans aucune perte d'information, cela n'est plus vrai pour les automates de Büchi \citep[][]{Buchi62,Piterman07}. Afin de s'affranchir de cette difficulté, on fait appel à un autre type d'automate sur mot infini.

%%%%%%%%%%% Une fois l'alphabet déterminé, on définit la \emph{trace} d'une exécution du système comme étant le mot constitué des caractères de l'alphabet et décrivant les états successifs parcourus par le système durant son exécution.

%%%%%%%%%%% On peut décrire le comportement d'un système en définissant son langage, c'est-à-dire l'ensemble de ses traces d'exécution. On notera habituellement $L$ le langage du système. Dans la suite, sauf mention contraire, on considère des systèmes dont les traces sont infinies~: $L \subseteq \Sigma\om$. 


\begin{mydef}[\acf{DPA}]\label{def:DPA}
Un \acf{DPA} est un tuple \[\mathcal{A}=(Q,\Sigma,\delta,q_0,F)\] tel que 
\begin{itemize}
\item $Q$ est un ensemble fini d'états, dont $q_0$ qui est l'état initial ;
\item $\Sigma$ est un alphabet ;
\item $\delta~: Q\times\Sigma \rightarrow Q$ est une fonction de transition ; 
\item $F$ est une fonction qui associe à chaque état une couleur parmi un ensemble fini $\{1,\dots,k\}$.
\end{itemize}
\end{mydef}

Un mot accepté par l'automate est un mot de $\Sigma\om$ telle que, lors de la lecture, la couleur minimale des états rencontrés infiniment souvent est paire. Autrement dit, si on note $\rho$ le mot considéré, et $Inf(\rho)$ l'ensemble des états rencontrés infiniment souvent durant la lecture, il reste à calculer $\min\{F(q)\,|\,q\in Inf(\rho)\}$. 

On parle de langage \emph{acceptant} ou \emph{reconnu} par un \ac{DPA} si l'ensemble des mots du langage constitue l'ensemble des mots reconnus par l'automate. Le langage est alors \emph{$\omega$-régulier}.

% \paragraph{Exemple de \ac{DPA}}
\begin{myex}
\begin{figure}[h]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state] (A)                     {$s_0|1$};
    \node[state]         (B) [above right of=A]        {$s_1|2$};
    \node[state]         (C) [below right of=A]        {$s_2|3$};

    \path (A) edge node {$a$} (B)
              edge node {$b,c$} (C)
          (B) edge [out=30,in=330,looseness=7] node {$a$} (B)
          	  edge node {$b,c$} (C)
          (C) edge [out=30,in=330,looseness=7] node {$a,b,c$} (C);

\end{tikzpicture}
\caption{Exemple de DPA vérifiant le langage $\omega$-régulier $L=a\om$, sur l'alphabet $\Sigma=\{a,b,c\}$}\label{exDPA}
\end{figure}

Sur l'exemple de la figure \ref{exDPA}, les états sont notés $q|F(q)$, avec les notations de la définition précédente. En analysant cet automate, on note que toute lecture d'un mot contenant un $b$ ou un $c$ envoie l'automate dans l'état $s_2$ indéfiniment. Ainsi, si $\rho$ est un mot qui n'appartient pas au langage $L=a\om$, c'est-à-dire si $\rho$ contient un $b$ ou un $c$, alors l'ensemble $Inf(\rho)$ est réduit à $\{s_2\}$, donc $\min\{F(q)\,|\,q\in Inf(\rho)\}=3$ donc est impair~: tout mot appartenant au complément du langage $L$ n'est pas reconnu par le \ac{DPA}. Inversement, la lecture de l'unique mot $a\om$ du langage $L$ reste indéfiniment dans l'état $s_1$. Ainsi, $\min\{F(q)\,|\,q\in Inf(a\om)\}=2$ donc est pair~: $a\om$ est reconnu par le \ac{DPA}. Ces deux affirmations permettent de conclure que le langage reconnu par le \ac{DPA} est le langage $L=a\om$.
\end{myex}
\FloatBarrier

% Notons que le \ac{DPA} n'est pas le seul type d'automate dont le langage acceptant est nécessairement $\omega$-régulier. Notamment, les automates de Büchi ou de Streett sont autant de modélisations que l'on pouvait choisir. Cependant, la théorie qui suit demande de pouvoir déterminiser les automates considérés~: \cite{Piterman07} explique une méthode de déterminisation de ces automates non-déterministes en \ac{DPA}.

% \begin{mydef}[Automate de Büchi]
% Un \emph{automate de Büchi} est un tuple $\mathcal{A}=(Q,\Sigma,\delta,q_0,F)$ tel que 
% \begin{itemize}
% \item $Q$ est un ensemble fini d'états, dont $q_0$ qui est l'état initial ;
% \item $\Sigma$ est un alphabet ;
% \item $\delta~: Q\times\Sigma \rightarrow Q$ est une fonction de transition ; 
% \item $F$ est un ensemble d'états acceptants.
% \end{itemize}
% \end{mydef}

% Dans un automate de Büchi, une exécution est acceptée si, et seulement si, elle passe indéfiniment souvent par des états de $F$.

\subsection{Les systèmes de transitions}
% partir de LTS
Alors que la partie précédente introduit davantage un outil mathématique, le but de cette partie est de définir les moyens possibles pour modéliser un système réel \citep[][]{BaierKatoen2008}.

La modélisation d'un système passe tout d'abord par l'abstraction de ses composantes inutiles pour l'étude -- par exemple, lors de l'étude d'un protocole de contrôle d'accès, on suppose généralement que la cryptographie est parfaite et incassable. Ainsi, on ne s'intéresse qu'aux aspects fonctionnels intéressants pour l'étude. Un système est alors considéré suivant deux aspects particuliers~: l'état de ses différentes propriétés atomiques, et son comportement futur. Ces deux aspects sont représentés dans un système d'états-transitions.

\begin{mydef}[\acf{LTS}]
Un \acf{LTS} est un tuple $\mathcal{A}=(Q,q_0,T,\Sigma,\lambda)$ tel que~:
\begin{itemize}
\item $Q$ est un ensemble dénombrable (fini ou non) d'états, avec $q_0$ l'état initial ;
\item $T\subseteq Q\times Q$ est une relation de transition ;
\item $\Sigma$ est un alphabet ;
\item $\lambda:Q\rightarrow \Sigma$ est une fonction d'étiquetage des états.
\end{itemize}
\end{mydef}

Ce modèle est appelé à temps discret. À chaque unité de temps, le système réalise une transition issue de son état courant, ce qui met alors à jour son état. On appelle exécution du modèle à partir d'un état $q\in Q$ la suite d'états $\rho=q_0q_1q_2\dots$, avec $q_0=q$ et $\forall i,\,(q_i,q_{i+1})\in T$ -- autrement dit, la transition entre $q_i$ et $q_{i+1}$ existe. La \emph{trace} de l'exécution $\rho$ est alors la suite $tr(\rho) = \lambda(q_0)\lambda(q_1)\lambda(q_2)\dots \in \Sigma^\infty$. 

Cette représentation s'apparente à un arbre d'exécutions non-déterministe, en ce sens qu'elle liste toutes les exécutions possibles du système, sans considérer si l'une d'elles est plus ou moins probable.

% \paragraph{Exemple de \ac{LTS}}
\begin{myex}
\begin{figure}[h]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={$q_0$}] (A)                     {$a$};
    \node[state,label={$q_1$}]         (B) [above right of=A]        {$b$};
    \node[state,label={$q_2$}]         (C) [below right of=A]        {$c$};

    \path (A) edge node {} (B)
              edge node {} (C)
          (B) edge [out=30,in=330,looseness=7] node {} (B)
          (C) edge [out=30,in=330,looseness=7] node {} (C);

\end{tikzpicture}
\caption{Exemple de LTS}\label{exLTS}
\end{figure}

L'exemple de la figure \ref{exLTS} modélise un système possédant deux exécutions infinies possibles~: $\rho_1=q_0q_1^\omega$ ou $\rho_2=q_0q_2^\omega$. Les traces sont respectivement $tr(\rho_1)=ab^\omega$ et $tr(\rho_2)=ac^\omega$.
\end{myex}
\FloatBarrier

\paragraph{Probabilisation}
% puis MC/PTS
L'aspect probabiliste a pour but d'affiner la modélisation du système en indi\-quant quelles exécutions sont les plus probables. De plus, ceci ajoute un outil de mesure. La probabilisation d'un \ac{LTS} s'effectue en attribuant des probabilités aux scénarios futurs suivant le passé du système. 

Formellement, prenons un \ac{LTS} $\mathcal{A}$ dont les états sont indicés par $q_i$ ($i\in\mathbb{N}$). Notons $X_k$ la variable aléatoire qui désigne l'état du système à l'instant discret $k$ ($k\in\mathbb{N}$). Probabiliser $\mathcal{A}$, c'est définir les probabilités $\prob(X_{k+1}=q_i\,|\,\bigwedge_{j=0}^{j=k}\,X_j=q_{n_j})$, pour tout $k\in\mathbb{N}$ un instant, $i\in\mathbb{N}$ l'indice d'un état, et $\rho=q_{n_0}\dots q_{n_k}$ une exécution.

Généralement, on considère que les systèmes probabilistes sont \emph{sans-mémoire}, c'est-à-dire que la probabilité de rencontrer un certain état à l'instant $k+1$ ne dépend que de l'état du système à l'instant présent $k$. On parle également d'hypothèse \emph{markovienne}. Formellement, définir les probabilités d'un système markovien, c'est définir les probabilités $\prob(X_{k+1}=q_i\,|\,X_k=q_j)$, pour tout instant $k$ et tout couple d'états $q_i,q_j\in Q$ ; autrement dit, c'est exactement définir les probabilités des transitions entre les états $q_i$ et $q_j$.

\begin{mydef}[\acf{MC}]
Une \acf{MC} est un tuple $\mathcal{A}=(Q,q_0,\Delta)$ tel que~:
\begin{itemize}
\item $Q$ est un ensemble dénombrable (fini ou non) d'états, avec $q_0$ l'état initial ;
\item $\Delta:Q\rightarrow \dist(Q)$ est une fonction qui associe à chaque état $q\in Q$ une distribution $\Delta(q)$ sur $Q$.
\end{itemize}
\end{mydef}

Le formalisme des \ac{MC} mis en relation avec nos réflexions sur les propriétés atomiques que l'on retrouve dans les \ac{LTS} permet de définir une extension à ces deux notions.

\begin{mydef}[\acf{PTS}]
Un \acf{PTS} est un tuple $\mathcal{A}=(Q,q_0,\Sigma,\Delta,L)$ tel que~:
\begin{itemize}
\item $Q$ est un ensemble dénombrable (fini ou non) d'états, avec $q_0$ l'état initial ;
\item $\Delta:Q\rightarrow \dist(Q)$ est une fonction qui associe à chaque état $q\in Q$ une distribution $\Delta(q)$ sur $Q$ ;
\item $\Sigma$ est un alphabet ;
\item $L:Q\rightarrow\Sigma$ est une fonction d'étiquetage des états.
\end{itemize}
\end{mydef}

Ainsi, avec la notion de \ac{PTS}, nous possédons le formalisme nécessaire pour modéliser des systèmes probabilistes. On considère qu'un système représenté par un \ac{PTS} est entièrement spécifié -- en oubliant les abstractions initiales.

% \paragraph{Exemple de \ac{PTS}}
\begin{myex}
\begin{figure}[h]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={$q_0$}] (A)                     {$a$};
    \node[state,label={$q_1$}]         (B) [above right of=A]        {$b$};
    \node[state,label={$q_2$}]         (C) [below right of=A]        {$c$};

    \path (A) edge node {0.2} (B)
              edge node {0.8} (C)
          (B) edge [out=30,in=330,looseness=7] node {1} (B)
          (C) edge [out=30,in=330,looseness=7] node {1} (C);

\end{tikzpicture}
\caption{Exemple de PTS}\label{exPTS}
\end{figure}

L'exemple de la figure \ref{exPTS} modélise le même système que l'exemple de la figure \ref{exLTS}, que l'on a probabilisé. Dans l'état initial, le système a désormais 80\% de chance d'aller dans l'état $q_2$ ou bien 20\% de chance d'aller dans l'état $q_1$. On note que cette figure représente bien un \ac{PTS} puisque chaque état induit une distribution sur $Q$.
\end{myex}
\FloatBarrier

\subsection{Un modèle de spécification}\label{ssec:MDP}
% MDP 
La section précédente introduit les outils nécessaires pour représenter complètement un système réel probabiliste. Cependant, une telle approche peut mener à une modélisation trop précise, qui ne laisse pas de place à l'ajustement. Par exemple, on peut vouloir ajuster les distributions dans certains états afin d'affiner le comportement ou le rapprocher du comportement désiré. Pour cela, on introduit des outils permettant une modélisation avec un niveau d'abstraction supplémentaire. On appelle ces nouveaux modèles des \emph{spécifications}, car le but est de spécifier le champ des possibles pour le système que l'on veut étudier. Les \ac{PTS} auxquels on aboutit après étude des spécifications sont des \emph{implémentations} de celles-ci. 

%%%% TRANSITION
Une première idée à envisager lorsqu'il s'agit d'élargir le champ des possibles est de proposer plusieurs choix de distributions pour chaque état. Implémenter un tel objet consiste alors à donner un poids plus ou moins important à chaque distribution au choix.

\begin{mydef}[\acf{MDP}]\label{def:MDP}
Un \acf{MDP} est un tuple $\mathcal{M}=(Q,q_0,A,\Delta,\Sigma,L)$ tel que~:
\begin{itemize}
\item $Q$ est un ensemble dénombrable (fini ou non) d'états, avec $q_0$ l'état initial ;
\item $A$ est un ensemble de choix de distributions sur $Q$ -- on l'appelle aussi l'ensemble des distributions de base ;
\item $\Delta:Q\times A\rightarrow \dist(Q)$ est une fonction qui associe à chaque état $q\in Q$ et à chaque choix $\mu\in A$ une distribution $\Delta(q,\mu)$ sur $Q$ ;
\item $\Sigma$ est un alphabet ;
\item $L:Q\rightarrow\Sigma$ est une fonction d'étiquetage des états.
\end{itemize}
\end{mydef}

Un \ac{MDP} est donc un \ac{PTS} pour lequel on propose un choix entre plusieurs distributions dans chaque état. C'est un premier niveau d'abstraction qui permet de s'affranchir des probabilités fixes des \ac{PTS}.

% \paragraph{Exemple de \ac{MDP}}
\begin{myex}
La figure \ref{exMDP} représente un modèle similaire à celui représenté par le \ac{PTS} de la figure \ref{exPTS}. La différence réside dans le fait que l'on a désormais le choix entre deux systèmes distincts, représentés par les deux distributions $\mu_1$ et $\mu_2$. L'implémentation consiste à choisir deux réels $\alpha$ et $\beta$, positifs et tels que leur somme vaut 1, afin de créer la distribution $\mu = \alpha\cdot\mu_1+\beta\cdot\mu_2$. On note qu'en choisissant $\alpha=1$ et $\beta=0$, on obtient le \ac{PTS} présenté figure \ref{exPTS}.

\begin{figure}[h]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={[shift={(-0.2,0)}]{$q_0$}}] (A)                     {$a$};
    \node[state,label={$q_1$}]         (B) [above right of=A]        {$b$};
    % \node[state]         (C) [label={[shift={(0,-1.7)}]$\emptyset$},below right of=A]        {$q_2$};
    \node[state]         (C) [label=below:$q_2$,below right of=A]        {$c$};

    \path (A) edge [bend left] node {$\mu_1,0.2$} (B)
              edge [bend left] node {$\mu_2,0.5$} (C)
              edge [bend right,right] node {$\mu_2,0.5$} (B)
              edge [bend right,below left] node {$\mu_1,0.8$} (C)
          (B) edge [out=30,in=330,looseness=7] node {$\mu_3,1$} (B)
          (C) edge [out=30,in=330,looseness=7] node {$\mu_4,1$} (C);

\end{tikzpicture}


\caption{Exemple de MDP}\label{exMDP}
\end{figure}
\end{myex}
\FloatBarrier

Le chapitre \ref{sec:Theme1} a pour but d'introduire un nouveau modèle de spécification, inspiré en partie des \ac{MDP}, à partir duquel nous définirons plus précisément le concept de raffinement. 

Avant cela, il est nécessaire d'introduire les notions de sécurité que nous utilisons pour décrire les systèmes dans cette étude.


%%% Transition

\section{Vérification de l'opacité}\label{sec:plus-op}
% on vérifie dans tous les objets
La formalisation de la section précédente permet la modélisation, entre autres, de systèmes sécurisés -- autrement dit, on désire vérifier un certain nombre de propriétés de sécurité à l'étape de modélisation du système. L'idée est ainsi de détecter les potentielles failles de sécurité avant même leur implémentation. Dans cette étude, l'accent sécuritaire est porté par la propriété d'opacité. 

Le scénario général que l'on étudie ici est le suivant. Soit un système $\mathcal{A}$ dont l'ensemble des propriétés atomiques constitue l'alphabet $\Sigma$. On considère un langage $\omega$-régulier $\varphi\in \Sigma\om$ que l'on désire garder secret. On dit qu'une exécution du système satisfait le secret si sa trace appartient au langage $\varphi$. On définit également une fonction d'observation, $\mathcal{O}:\Sigma^{\infty} \rightarrow \Sigma^{\infty}_{ob}$, avec $\Sigma_{ob}$ la partie observable de l'alphabet $\Sigma$. On se limite aux projections naturelles de $\Sigma$ dans $\Sigma_{ob}$, généralisées aux langages $\omega$-réguliers \citep[][]{Mullins14,main}, c'est-à-dire les fonctions qui associent le mot vide à chaque élément non-observable de $\Sigma$, qui laissent inchangé tout élément observable de l'alphabet, et telles que $\mathcal{O}(\varepsilon)=\varepsilon$~: 
\[
\begin{array}{lrcl}
\forall \sigma\in\Sigma^{\infty},\,\forall a\in\Sigma\setminus\Sigma_{ob},\,& \mathcal{O}(\sigma a)&=&\mathcal{O}(\sigma)\\
\forall \sigma\in\Sigma^{\infty},\,\forall b\in\Sigma_{ob},\,& \mathcal{O}(\sigma b)&=&\mathcal{O}(\sigma) b.
\end{array}
\]
 On note $[\sigma]_{\mathcal{O}}$ -- ou $[\sigma]$ quand l'observateur est sous-entendu -- la classe d'observation du mot $\sigma$, c'est-à-dire l'ensemble des mots dont l'observation par $\mathcal{O}$ est la même que celle du mot $\sigma$~:
\[[\sigma]_{\mathcal{O}} = \mathcal{O}^{-1}(\mathcal{O}(\sigma)).\]
On note $Obs$ l'ensemble des classes d'observations de la fonction d'observation.

Notons que si $L$ est un langage $\omega$-régulier et si $\mathcal{O}$ est un observateur rationnel tel que présenté ci-dessus, alors le langage $\mathcal{O}(L)$ est un langage $\omega$-régulier \citep[][]{BMS15}.

Le but est de calculer la capacité pour le système $\mathcal{A}$ de cacher l'exécution du secret à l'observateur rationnel extérieur.









\subsection{Une opacité binaire}

La première définition d'opacité s'applique dans le contexte des \ac{LTS}. Formellement, on dit d'un secret qu'il est opaque si, pour toute exécution du secret, il existe une exécution non-secrète qui est observée de la même manière par l'observateur rationnel extérieur -- on dit que le secret est couvert par une exécution non-secrète. Autrement dit, pour que le secret soit opaque, il faut et il suffit que chaque classe d'observation contenant une exécution secrète contienne également au moins une exécution non-secrète. On en déduit la définition suivante.

\begin{mydef}[Opacité]\label{def:opbin}
Soient un \ac{LTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. Le secret $\varphi$ est \emph{opaque dans $\mathcal{A}$ relativement à $\mathcal{O}$} si, et seulement si,
\[ \mathcal{O}(\varphi) \subseteq \mathcal{O}(L\setminus\varphi).\]
\end{mydef}

Notons que l'on peut également définir l'opacité symétrique, en affirmant qu'un secret $\varphi$ est symétriquement opaque dans un système au langage $L$ relativement à un observateur si, et seulement si, $\varphi$ et $L\setminus\varphi$ sont opaques dans le système relativement à l'observateur. Autrement dit, il faut et il suffit que chaque classe d'observation possède à la fois des exécutions secrètes et non-secrètes.

Cette première définition d'opacité induit une classification simple de problèmes de sécurité, puisque l'opacité ici est une grandeur binaire~: soit le secret est opaque, soit il ne l'est pas. Cependant, dire que le secret n'est pas opaque signifie qu'il existe une exécution appartenant à l'ensemble $\varphi$ telle qu'aucune exécution non-secrète ne possède la même observation. Par conséquent, cela ne signifie pas que le secret est systématiquement trahi. Cela signifie uniquement que, si l'exécution en question est observée, alors l'observateur a pleine connaissance du fait qu'il est en présence d'un secret. La question naturelle que l'on peut se poser alors est la suivante~: quelle est la probabilité que cette exécution soit réalisée par le système ?














\subsection{L'opacité probabiliste}

\subsubsection{L'opacité probabiliste libérale}
Afin de répondre à cette question, il faut pouvoir inclure l'aspect probabiliste au modèle. Pour cela, on adapte la notion d'opacité au contexte des \ac{PTS}. 

On considère l'ensemble des exécutions du système pour lesquelles un observateur est certain d'être en présence du secret. Cet ensemble est donc l'ensemble des exécutions du secret qui n'appartiennent à aucune classe d'observation d'une exécution non-secrète~: autrement dit, avec les notations précédentes, on considère l'ensemble $\mathcal{V}(\mathcal{A},\varphi,\mathcal{O})=\varphi\, \cap \,\overline{\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))}$ \citep[][]{BMS15,Sassolas2011}. 

\begin{mydef}[Opacité libérale]
Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. L'\emph{opacité libérale de $\varphi$ dans $\mathcal{A}$ relativement à $\mathcal{O}$} est la grandeur~:
\[ PO_l(\mathcal{A},\varphi,\mathcal{O}) = \prob \big(\varphi \cap \overline{\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))}\big).\]
\end{mydef}

Cette définition probabiliste nuance la définition binaire d'opacité, en ce sens qu'elle permet de classer différents problèmes de sécurité suivant la valeur obtenue. 

\begin{prop}\label{P:pol}
Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$.
\begin{itemize}
\item $0\leq PO_l(\mathcal{A},\varphi,\mathcal{O})\leq 1$ ;
\item $PO_l(\mathcal{A},\varphi,\mathcal{O})=1$ si, et seulement si, $\varphi=L$ ;
\item $PO_l(\mathcal{A},\varphi,\mathcal{O})=0$ si, et seulement si, $\varphi$ est opaque dans $\mathcal{A}$ relativement à $\mathcal{O}$.
\end{itemize}
\end{prop}

\begin{proof}
Le premier point découle du fait que l'opacité libérale est par définition une probabilité.

Pour montrer le deuxième point, 
remarquons que si $X$ est un ensemble, $\prob(X)=1$ si, et seulement si, $X=U$, avec $U$ l'univers. Par conséquent, 
\[
\begin{array}{rclcrcl}
PO_l(\mathcal{A},\varphi,\mathcal{O})&=&1 & \Leftrightarrow & \mathcal{V}(\mathcal{A},\varphi,\mathcal{O})&=&L\\
&&& \Leftrightarrow & \varphi\cap\overline{\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))}&=&L \\
&&& \Leftrightarrow & L\subseteq\varphi & \wedge & L\subseteq\overline{\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))}\\
&&& \Leftrightarrow & L &=& \varphi \;\;\;\; (\mbox{car } \varphi\subseteq L)
\end{array}
\]
ce qui prouve le deuxième point.

Pour montrer le troisième point, remarquons que si $X$ est un ensemble, $\prob(X)=0$ si, et seulement si, $X=\emptyset$. Par conséquent, 
\[
\begin{array}{rclcrcl}
PO_l(\mathcal{A},\varphi,\mathcal{O})&=&0 & \Leftrightarrow & \mathcal{V}(\mathcal{A},\varphi,\mathcal{O})&=&\emptyset\\
&&& \Leftrightarrow & \mathcal{O}(\varphi)\cap\overline{\mathcal{O}(L\setminus\varphi)}&=&\emptyset \\
&&& \Leftrightarrow & \mathcal{O}(\varphi)&\subseteq&\mathcal{O}(L\setminus\varphi)
\end{array}
\]
ce qui prouve la propriété.
\end{proof}

Cette valeur permet de discriminer les systèmes pour lequel les secrets sont non-opaques entre eux. On peut alors affirmer quels sont les systèmes les moins sécurisés, c'est-à-dire ceux pour lesquels la probabilité de trahir tout le secret est la plus grande. 

Notons qu'à l'inverse des propriétés de flux d'information fondées sur la notion d'entropie de Shannon \citep[][]{Smith09}, on ne calcule pas la proportion du secret qui n'est plus protégée, mais bien la probabilité que tout le secret soit rendu public. 

%% POl est décidable
\begin{mythm}
Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. Alors la grandeur $PO_l(\mathcal{A},\varphi,\mathcal{O})$ est mesurable.
\end{mythm}

\begin{proof}
D'après la proposition \ref{prop:reg} appliquée au cas particulier d'un \ac{PTS}, le langage $L$ est $\omega$-régulier. De même, $\varphi$ est $\omega$-régulier. Enfin, d'après la remarque sur les observateurs rationnels, et d'après les propriétés de fermeture des langages $\omega$-réguliers, on en déduit que le langage dont on veut connaître la probabilité est $\omega$-régulier. Ainsi, le problème de calcul d'opacité libérale revient à un problème de calcul de probabilité d'un langage $\omega$-régulier dans un \ac{PTS}~: on sait que ce problème est décidable. %%%%%%%%%%%%%%% REFERENCE
\end{proof}









\subsubsection{D'autres formes d'opacité} 
%restrictive 
\paragraph{Opacité restrictive}
À l'instar de l'opacité libérale qui permet de discriminer les systèmes pour lesquels les secrets sont non-opaques, on peut définir une notion duale afin de discriminer les systèmes pour lesquels les secrets sont opaques. 

Intuitivement, le fait que le secret soit opaque ne le rend pas totalement immune à toute fuite. En effet, l'observation donne un certain nombre d'informations à l'attaquant extérieur. Celui-ci connaît la répartition du secret sur les classes~: ainsi, s'il observe une classe couverte en grande majorité par des exécutions secrètes, sa probabilité de pouvoir affirmer qu'il est en présence du secret est plus importante que s'il est en présence d'une classe majoritairement couverte par des exécutions non-critiques \citep[][]{BMS15,Sassolas2011}.

\begin{mydef}[Opacité restrictive]
Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. L'\emph{opacité restrictive de $\varphi$ dans $\mathcal{A}$ relativement à $\mathcal{O}$} est la grandeur définie par~:
\[ \frac{1}{PO_r(\mathcal{A},\varphi,\mathcal{O})} = \sum_{o\in Obs} \prob(o)\cdot\frac{1}{\prob(L\setminus\varphi\,|\,o)}. \]
\end{mydef}

On utilise la moyenne harmonique pondérée par les probabilités sur les classes d'observation afin de donner davantage de poids aux classes qui ont le plus de chance de trahir le secret. De plus, cette définition vérifie les résultats de la proposition \ref{propOR}, qui sont les résultats naturels d'une définition d'opacité restrictive.

\begin{prop}\label{propOR}
Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. 
\begin{itemize}
\item $0\leq PO_r(\mathcal{A},\varphi,\mathcal{O})\leq 1$ ;
\item $PO_r(\mathcal{A},\varphi,\mathcal{O})=0$ si, et seulement si, $\varphi$ n'est pas opaque, c'est-à-dire qu'il existe une classe d'observation qui est uniquement composée d'exécutions de $\varphi$ ;
\item $PO_r(\mathcal{A},\varphi,\mathcal{O})=1$ si, et seulement si, $\varphi=\emptyset$.
\end{itemize}
\end{prop}

\begin{proof}
Le premier point se déduit immédiatement du fait que l'opacité restrictive est par définition une moyenne harmonique de plusieurs probabilités, c'est donc une probabilité également.

Pour montrer le second point, remarquons que $\varphi$ n'est pas opaque si, et seulement si, il existe une classe d'observation $o\in Obs$ composée uniquement d'exécutions de $\varphi$, \emph{i.e.} telle que $\prob(L\setminus\varphi\,|\,o)=0$. Cela est équivalent au fait que $\frac{1}{PO_r(\mathcal{A},\varphi,\mathcal{O})}$ tend vers l'infini, car alors au moins un des termes de la somme tend vers l'infini. Or, l'inverse de l'opacité tend vers l'infini si, et seulement si, l'opacité restrictive tend vers 0, ce qui termine la preuve.

Montrons le dernier point.
\[
\begin{array}{rclcrrcl}
PO_r(\mathcal{A},\varphi,\mathcal{O})&=&1 & \Leftrightarrow & &\frac{1}{PO_r(\mathcal{A},\varphi,\mathcal{O})}&=&1 \\
&&& \Leftrightarrow &  \forall o\in Obs,\,& \prob(L\setminus\varphi\,|\,o)&=&\prob(o) \\
% &&& \Leftrightarrow & & \sum{o\in Obs} \prob(L\setminus\varphi\,|\,o)&=&1\\
&&& \Leftrightarrow & & \prob(L\setminus\varphi) &=& 1\\
&&& \Leftrightarrow & & \varphi &=& \emptyset.
\end{array}
\]
\end{proof}



% \newpage

\paragraph{Un modèle alternatif}
%hadjicostis x2  !!!! SI FIN INUTILE, MODIFIER LES NOMBRES !!!!!
On présente dans la suite de cette section une mesure d'opacité alternative \citep[][]{Saboori2014}. Dans leurs travaux, les auteurs considèrent des systèmes finis -- dont les langages sont inclus dans $\Sigma^*$. On propose de transférer leurs notions dans la sémantique des langages de $\Sigma\om$. Pour cela, on définit la restriction suivante, inspirée des notations de l'article en question.

\begin{mydef}
Soit $L$ un langage de $\Sigma^*$.
\begin{itemize}%%%%% SI OPACITÉ 2 INUTILE~: on retire points 1 et 2
% \item L'ensemble des préfixes de $L$ est noté $Pref(L)=\{u\in\Sigma^*\,|\,\exists v\in\Sigma^*\,:\,uv\in L\}$.
% \item L'ensemble des mots de $L$ dont on a retiré le dernier caractère est noté $$\Sigma^{-1}L=\{u\in\Sigma^*\,|\,\exists a\in\Sigma\,:\,ua\in L\}$$.
\item L'extension de $L$ à $\Sigma\om$ est le cône noté $L_{\omega}=L\Sigma\om$ ;
\item Si $\sigma$ est un mot de $L$, on appelle \emph{longueur utile} du cône résultant $\sigma_{\omega}$ la longueur de la chaîne $\sigma$ ; $\sigma$ est alors appelé \emph{partie utile}.
\item Si $\mathcal{A}$ est un \ac{PTS} dont le langage des exécutions est $L$, on construit $\mathcal{A}_{\omega}$ le \ac{PTS} dont le langage des exécutions est $L_{\omega}$.
\end{itemize}
\end{mydef}

%%
%% À INCLURE AU BON MOMENT
%%
%% \begin{mythm}
%% Si $L$ est un langage régulier, alors $L_{\omega}$ est un langage $\omega$-régulier.
%% \end{mythm}
%%

L'objet de cette partie concerne donc un ensemble très restreint de \ac{PTS}.

Considérons que l'attaquant n'est capable d'observer l'exécution que durant un temps fini, qu'il peut choisir aussi long qu'il le désire. Ainsi, il n'est capable d'observer que les exécutions dont la longueur utile est fixée à un entier $k$, correspondant au temps d'observation. Pour vérifier qu'un système est bien sécurisé contre ce type d'observation, il est alors nécessaire de vérifier que c'est le cas pour chaque longueur potentiellement choisie par l'attaquant. En substance, on introduit ici la notion de quasi-opacité~: on autorise le fait que le secret ne soit pas opaque, mais on veut s'assurer que l'opacité libérale ne dépasse jamais un seuil critique, noté $\theta$.

\begin{mydef}[Quasi-opacité uniforme]\label{Def:QOU}
Soit $L$ un langage de $\Sigma^*$. Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. 

Notons $L_C=\varphi\setminus\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))$, le langage de $\Sigma^*$ composé des exécutions de $\mathcal{A}$ qui brisent l'opacité de $\varphi$.

Alors, pour un $\theta>0$, le cône du secret $\varphi_{\omega}$ est \emph{uniformément quasi-opaque, ou $\theta$-opaque, dans $\mathcal{A}_{\omega}$ relativement à $\mathcal{O}$} si, et seulement si,
\[ \forall k\in\mathbb{N},\,\prob\big((L_C\,\cap\,\Sigma^k)_{\omega}\big)<\theta.\]
\end{mydef}

Il est important de noter que le langage $L_C$ est construit à partir de la définition de l'opacité dans la sémantique des langages réguliers sur $\Sigma^*$ \citep[][]{Mullins14}. Formellement, les notions sont analogues, et $L_C$ est exactement le langage $\mathcal{V}(\mathcal{A},\varphi,\mathcal{O})$ dont on calcule la probabilité dans le cadre de l'opacité libérale. La notion de quasi-opacité uniforme revient donc à dire que l'opacité libérale du secret est uniformément répartie suivant la longueur utile des mots du langage $(L_C)_{\omega}$. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                  INUTILE ???!!!!!??????????
% La seconde mesure d'opacité inspirée de \cite{Saboori2014} correspond au scénario suivant. L'observateur est désormais capable d'observer les mots de longueur utile quelconque et variable, sans restriction aucune, tel que dans le scénario habituel de l'opacité. Les auteurs remarquent alors que, si $\sigma$ et $\tau$ sont des mots finis qui brisent l'opacité de $\varphi$ et tels que $\sigma$ est un préfixe de $\tau$, alors il est inutile de prendre en compte la probabilité de $\tau$ dans le calcul de l'opacité. En effet, pour que $\tau$ soit réalisé, il faut que $\sigma$ soit observé auparavant~: le secret $\varphi$ est déjà brisé. Ainsi, il suffit de limiter les calculs aux mots qui n'ont aucun préfixe dans l'ensemble qui brise l'opacité. 

% Lorsque l'on transpose ces notions dans le langage $L_{\omega}$, on considère les mots $\sigma_{\omega}$ tels que leurs parties utiles n'ont aucun préfixe dans l'ensemble des mots qui brisent l'opacité. 

% \begin{mydef}[Opacité libérale non-préfixée]
% Soit $L$ un langage de $\Sigma^*$. Soient un \ac{PTS} $\mathcal{A}$ dont le langage des exécutions est $L$, un secret régulier $\varphi\subseteq L$ et un observateur rationnel $\mathcal{O}$. 

% Notons $L_C=\varphi\setminus\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))$, le langage de $\Sigma^*$ composé des exécutions de $\mathcal{A}$ qui brisent l'opacité de $\varphi$.

% Notons $L_C^P=L_C\setminus Pref(\Sigma^{-1}L_C)$, l'ensemble des mots de $L_C$ qui n'ont aucun préfixe strict dans $L_C$.

% Alors, l'\emph{opacité libérale non-préfixée de $\varphi_{\omega}$ dans $\mathcal{A}$ relativement à $\mathcal{O}$} est la grandeur
% \[ PO_l^P(\mathcal{A},\varphi,\mathcal{O})=\prob\big((L_C^P)_{\omega}\big)\].
% \end{mydef}

% On remarque que cette définition est exactement la mesure d'opacité libérale du cas général. En effet, si $\sigma$ et $\tau$ sont deux mots de $\Sigma^*$ tels que $\sigma$ est un préfixe de $\tau$, alors le langage $\sigma_{\omega}$ contient le langage $\tau_{\omega}$. %%%..................
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIN INUTILE





\paragraph{En résumé}
Ce chapitre a permis d'introduire les notions préliminaires nécessaires à la construction des théorèmes et contributions du chapitre suivant. Nous avons introduit des notions liées principalement au concept d'implémentation~: les systèmes décrits par les \ac{PTS} sont des systèmes très précis, notamment au niveau des probabilités de transitions. Le but du chapitre suivant est de s'affranchir de cette précision, afin de pouvoir utiliser le concept de raffinement de systèmes. Il est alors nécessaire d'analyser comment se comportent les propriétés de sécurité énoncées dans le cadre plus général des spécifications de systèmes.





