\beamer@endinputifotherversion {3.36pt}
\select@language {french}
\beamer@sectionintoc {1}{Cadre th\IeC {\'e}orique}{5}{0}{1}
\beamer@sectionintoc {2}{Algorithme de calcul d'opacit\IeC {\'e}}{12}{0}{2}
\beamer@sectionintoc {3}{Pr\IeC {\'e}servation de l'opacit\IeC {\'e} par raffinement}{25}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{29}{0}{4}
