\beamer@endinputifotherversion {3.36pt}
\select@language {french}
\beamer@sectionintoc {1}{Pr\IeC {\'e}liminaires}{13}{0}{1}
\beamer@sectionintoc {2}{Sp\IeC {\'e}cification et raffinement}{39}{0}{2}
\beamer@sectionintoc {3}{Calcul de l'opacit\IeC {\'e} dans les sp\IeC {\'e}cifications}{45}{0}{3}
\beamer@sectionintoc {4}{Pr\IeC {\'e}servation de l'opacit\IeC {\'e} par raffinement}{78}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{84}{0}{5}
