\documentclass{beamer} %% handout à supprimer si pas d'animation \pause

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[frenchb]{babel}
\usepackage{lmodern}

\usepackage[caption=false]{subfig}
\usepackage{xcolor,colortbl}

\usepackage{tikz}
\usetikzlibrary{arrows,automata,calc,positioning,shapes}
% ,fit}
% \usepackage{tikz-3dplot}
% \AtBeginEnvironment{tikzpicture}{{}
% \AtBeginEnvironment{figure}{{}
% \tikzset{
%     every picture/.prefix style={
%         execute at begin picture={
%     }
% }
\tikzstyle{every picture}+=[initial text=]
\tikzstyle{every state}+=[inner sep=0.6mm,minimum size=0em]


\newcommand{\gear}[6]{%
  (0:#2)
  \foreach \i [evaluate=\i as \n using {\i-1)*360/#1}] in {1,...,#1}{%
    arc (\n:\n+#4:#2) {[rounded corners=1.5pt] -- (\n+#4+#5:#3)
    arc (\n+#4+#5:\n+360/#1-#5:#3)} --  (\n+360/#1:#2)
  }%
  (0,0) circle[radius=#6] 
}

\makeatletter
\tikzset{
  through point/.style={
    to path={%
      \pgfextra{%
        \tikz@scan@one@point\pgfutil@firstofone(\tikztostart)\relax
        \pgfmathsetmacro{\pt@sx}{\pgf@x * 0.03514598035}%
        \pgfmathsetmacro{\pt@sy}{\pgf@y * 0.03514598035}%
        \tikz@scan@one@point\pgfutil@firstofone#1\relax
        \pgfmathsetmacro{\pt@ax}{\pgf@x * 0.03514598035 - \pt@sx}%
        \pgfmathsetmacro{\pt@ay}{\pgf@y * 0.03514598035 - \pt@sy}%
        \tikz@scan@one@point\pgfutil@firstofone(\tikztotarget)\relax
        \pgfmathsetmacro{\pt@ex}{\pgf@x * 0.03514598035 - \pt@sx}%
        \pgfmathsetmacro{\pt@ey}{\pgf@y * 0.03514598035 - \pt@sy}%
        \pgfmathsetmacro{\pt@len}{\pt@ex * \pt@ex + \pt@ey * \pt@ey}%
        \pgfmathsetmacro{\pt@t}{(\pt@ax * \pt@ex + \pt@ay * \pt@ey)/\pt@len}%
        \pgfmathsetmacro{\pt@t}{(\pt@t > .5 ? 1 - \pt@t : \pt@t)}%
        \pgfmathsetmacro{\pt@h}{(\pt@ax * \pt@ey - \pt@ay * \pt@ex)/\pt@len}%
        \pgfmathsetmacro{\pt@y}{\pt@h/(3 * \pt@t * (1 - \pt@t))}%
      }
      (\tikztostart) .. controls +(\pt@t * \pt@ex + \pt@y * \pt@ey, \pt@t * \pt@ey - \pt@y * \pt@ex) and +(-\pt@t * \pt@ex + \pt@y * \pt@ey, -\pt@t * \pt@ey - \pt@y * \pt@ex) .. (\tikztotarget)
    }
  }
}
\makeatother


\usetheme{Madrid}
\usecolortheme{beaver}


\definecolor{rouge}{HTML}{8A0808}
\setbeamercolor{titre}{bg=rouge,fg=white} % pour les blocs exemples
\setbeamercolor{texte}{bg=rouge!10,fg=black}

\setbeamercolor{item}{fg=red!70!black}
\setbeamertemplate{section in toc}{%
  {\color{red!70!black}\inserttocsectionnumber.}~\inserttocsection}

\beamertemplatenavigationsymbolsempty %% Pour supprimer barre navigation

% \setbeamercolor{subsection in toc}{bg=white,fg=structure}
% \setbeamertemplate{subsection in toc}{%
  % \hspace{1.2em}{\color{orange}\rule[0.3ex]{3pt}{3pt}}~\inserttocsubsection\par}

%% Commandes personnelles %%
\newcommand{\om}{\ensuremath{^{\omega}}}
\newcommand{\prob}{\ensuremath{{\bf P}}}
\newcommand{\Spec}{\ensuremath{\mathcal{S}}}
\newcommand{\satO}{\mathit{S\_imp}}
\newcommand{\sat}{\mathit{imp}}
\newcommand{\dist}{\ensuremath{\mathcal{D}ist}}
\newcommand{\rel}{\ensuremath{\mathcal{R}}}
\newcommand{\FExec}{\ensuremath{F\!Exec}}
\newcommand{\lst}{\mbox{lst}}
\newcommand{\polO}{\ensuremath{S\_PO_{l}}}

\newtheorem{mydef}{Définition}
\newtheorem*{myex}{Exemple}
\newtheorem*{rem}{Remarque}
\newtheorem{coro}{Corollaire}
\newtheorem{prop}{Proposition}
\newtheorem{mylem}{Lemme}
\newtheorem{mythm}{Théorème}






\title[]{Préservation de l'opacité par raffinement de systèmes spécifiés par des Chaînes de Markov discrètes à Intervalles}
\author[G. Dupeuble]{Gaëtan Dupeuble}
\institute[EPM]{Département de Génie Informatique et Génie Logiciel, École Polytechnique de Montréal}
\date{9 mai 2017}


\renewcommand{\thefootnote}{\arabic{footnote}}
\newcommand{\eps}{\varepsilon}


% \setbeamertemplate{background canvas}{\includegraphics[width=\paperwidth,height=\paperheight]{imagefond}}
\AtBeginSection[]% pour afficher sommaire à chaque début de section 
{
  \begin{frame}
  \frametitle{Sommaire}
  \tableofcontents[currentsection, hideothersubsections]
  \end{frame} 
}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{frame}
% \frametitle{\ref{}. }
% \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\begin{frame}
 \titlepage
\end{frame}


\begin{frame}
\frametitle{Introduction -- Conception par raffinement de modèles}
\begin{itemize}
  \item Requis : caractéristique souhaitée du système à concevoir
  \item Modèle : représentation formelle des requis du système
  \item Raffinement : processus de conception graduelle du modèle
  \item Implémentation : système issu du modèle complètement raffiné
\end{itemize}
\uncover<2->{
\begin{figure}
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5cm,semithick]
    \node[draw,align=center] (A) {Spécification des requis\\sous forme de modèle};
%     \node[align=center] (B) [below of=A] {Raffinement de modèle};
    % \node[state,accepting] (C) [right of=B] {$s_2$};
    \node[] (D) [right of=A] {Implémentation};
    
    \path (A) edge [out=-60,in=-120,looseness=7] node {Raffinement de modèle} (A)
%               edge node[swap] {$b,c$} (D)
          % (B) edge [red] node {$b$} (C)
          %     edge node {$a$} (D)
%           (B) edge [out=30,in=330,looseness=7,red] node {$b$} (B)
%               edge node {$a,c$} (D)
           edge  node {} (D);

    \end{tikzpicture}
\caption{Principe de la conception par raffinement de modèles}
\end{figure}
}
\uncover<3->{
{{\Large\bf Le raffinement doit préserver les requis à chaque étape.}}
}
~
\end{frame}

\begin{frame}
 \frametitle{Introduction -- Requis fonctionnels ou non}
Deux types de requis :
\begin{itemize}
 \item fonctionnels
 \begin{itemize}
  \item exemple : absence de famine entre deux processus concurrents 
 \end{itemize}
 \item non-fonctionnels
 \begin{itemize}
  \item exemple : disponibilité du service
 \end{itemize}
\end{itemize}

\uncover<2->{}

\end{frame}






%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Sommaire}
\tableofcontents[hideallsubsections]
~
\end{frame} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Préliminaires}\label{prelim}

\begin{frame}
\frametitle{\ref{prelim}. Automate infinitaire}
\begin{itemize}
  \item Reconnaît un langage $\omega$-régulier
  \item Déterministe
\end{itemize}
% \pause
\begin{figure}[h]
  \centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick]
    \node[initial,state] (A) {$s_0$};
    \node[state,accepting] (B) [right of=A] {$s_1$};
    % \node[state,accepting] (C) [right of=B] {$s_2$};
    \node[state] (D) [below of=B] {$s_2$};
    
    \path (A) edge [red] node {$a$} (B)
              edge node[swap] {$b,c$} (D)
          % (B) edge [red] node {$b$} (C)
          %     edge node {$a$} (D)
          (B) edge [out=30,in=330,looseness=7,red] node {$b$} (B)
              edge node {$a,c$} (D)
          (D) edge [out=30,in=330,looseness=7] node {$a,b,c$} (D);

    \end{tikzpicture}
    \caption{Automate de Büchi reconnaissant le langage $ab^\omega$}\label{fig:dpa}
    \end{figure}
    ~
\end{frame} 




\begin{frame}
\frametitle{\ref{prelim}. Système de transitions probabiliste (PTS)}
\begin{columns}[t]
\begin{column}{3.5cm}
\begin{itemize}
  \item Implémentation d'un modèle
  \item Déterministe
  \item Trace d'exécution infinie \\\only<2>{(ex : {\color{red}$\sigma=ab^\omega$})}
  \begin{itemize}
    \item Ensemble des traces : $Exec(S)$
  \end{itemize}
\end{itemize}
\end{column}


\begin{column}{7.5cm}
\begin{figure}[h]
\centering
\only<1>{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={$q_0$}] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state,label={$q_1$}]         (B) [above of=ph]        {$b$};
    \node[state,label={$q_2$}]         (C) [below of=ph]        {$c$};

    \path (A) edge node {0.2} (B)
              edge node[swap] {0.8} (C)
          (B) edge [out=30,in=330,looseness=7] node {1} (B)
          (C) edge [out=30,in=330,looseness=7] node {1} (C);

\end{tikzpicture}}

\only<2>{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={$q_0$},draw=red] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state,label={$q_1$},draw=red]         (B) [above of=ph]        {$b$};
    \node[state,label={$q_2$}]         (C) [below of=ph]        {$c$};

    \path (A) edge [red]node {0.2} (B)
              edge node[swap] {0.8} (C)
          (B) edge [out=30,in=330,looseness=7,red] node {1} (B)
          (C) edge [out=30,in=330,looseness=7] node {1} (C);

\end{tikzpicture}}
\caption{Exemple de PTS}
\end{figure}
\end{column}
\end{columns}
~
\end{frame} 



%%%%%%%%%%%%%%%%%%%%% Procédure de calcul de probabilité maximale de langage dans MDP
\begin{frame}
\frametitle{\ref{prelim}. Processus de Décision Markovien (MDP)}

\begin{columns}[t]
\begin{column}{5cm}
\begin{figure}[h]
\centering
{\small \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={[shift={(-0.2,0)}]{$q_0$}}] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state,label={$q_1$}]         (B) [above of=ph]        {$b$};
    % \node[state]         (C) [label={[shift={(0,-1.7)}]$\emptyset$},below right of=A]        {$q_2$};
    \node[state]         (C) [label=below:$q_2$,below of=ph]        {$c$};

    \path (A) edge [bend left,red] node {$\mu_1,0.2$} (B)
              edge [bend left,blue] node {$\mu_2,0.5$} (C)
              edge [bend right,right,blue] node {$\mu_2,0.5$} (B)
              edge [bend right,below left,red] node {$\mu_1,0.8$} (C)
          (B) edge [out=30,in=330,looseness=7] node {$\mu_3,1$} (B)
          (C) edge [out=30,in=330,looseness=7] node {$\mu_4,1$} (C);

\end{tikzpicture}
}\caption{Exemple de MDP $\mathcal{M}$}
\end{figure}
\end{column}
\begin{column}{5cm}

\only<1>{
  \begin{itemize}
    \item Spécification non-déterministe
    \item Implémentée par choix de distribution
    \begin{itemize}
      \item Une distribution valable = un \textbf{barycentre} des distributions de base
    \end{itemize}
  \end{itemize}
}
% \end{column}
% \end{columns}
% \end{frame} 


% %%%%%%frame intermédiaire sur les ordonnanceurs



% \begin{frame}
% \frametitle{\ref{prelim}. Processus de Décision Markovien (MDP)}
% \begin{columns}[t]
% \begin{column}{5cm}
% \begin{figure}[h]
% \centering
% {\small \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
%       \tikzstyle{every state}=[fill=white,text=black]

%     \node[initial,state,label={[shift={(-0.2,0)}]{$q_0$}}] (A)                     {$a$};
%     \node (ph) [right of=A] {};
%     \node[state,label={$q_1$}]         (B) [above of=ph]        {$b$};
%     % \node[state]         (C) [label={[shift={(0,-1.7)}]$\emptyset$},below right of=A]        {$q_2$};
%     \node[state]         (C) [label=below:$q_2$,below of=ph]        {$c$};

%     \path (A) edge [bend left] node {$\mu_1,0.2$} (B)
%               edge [bend left] node {$\mu_2,0.5$} (C)
%               edge [bend right,right] node {$\mu_2,0.5$} (B)
%               edge [bend right,below left] node {$\mu_1,0.8$} (C)
%           (B) edge [out=30,in=330,looseness=7] node {$\mu_3,1$} (B)
%           (C) edge [out=30,in=330,looseness=7] node {$\mu_4,1$} (C);

% \end{tikzpicture}
% }\caption{Exemple de MDP $\mathcal{M}$}
% \end{figure}
% \end{column}
% \begin{column}{5cm}
\only<2>{

\begin{figure}[h]
\centering
\subfloat[Implémente $\mathcal{M}$]{
\tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state,label={$q_0$}] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state,label={$q_1$}]         (B) [above right of=A]        {$b$};
    \node[state,label={$q_2$}]         (C) [below right of=A]        {$c$};
    \node (E) [right of=ph] {};

    \path (A) edge node {0.4} (B)
              edge node[swap] {0.6} (C)
          (B) edge [out=30,in=330,looseness=7] node {1} (B)
          (C) edge [out=30,in=330,looseness=7] node {1} (C);

\end{tikzpicture}}}

\subfloat[N'implémente pas $\mathcal{M}$]{
\tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state,label={$q_0$}] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state,label={$q_1$}]         (B) [above right of=A]        {$b$};
    \node[state,label={$q_2$}]         (C) [below right of=A]        {$c$};
    \node (E) [right of=ph] {};

    \path (A) edge node[red] {0.6} (B)
              edge node[red,swap] {0.4} (C)
          (B) edge [out=30,in=330,looseness=7] node {1} (B)
          (C) edge [out=30,in=330,looseness=7] node {1} (C);
\end{tikzpicture}}}
\end{figure}
}

\only<3>{
  Calcul du maximum de probabilité d'un langage :
  \begin{itemize}
    \item maximum atteint sur une implémentation issue des distributions de base
    % \item test
    \begin{itemize}
      \item ex : $\prob_{max}(ab^\omega) =0.5$
      % \item test
    \end{itemize}
  \end{itemize}
}

\end{column}
\end{columns}
~
\end{frame} 



%%%%%%%%%%%%%%%%%%%%% Calques successifs ? Œil en tikz ?
%%% source : https://tex.stackexchange.com/questions/94082/help-with-the-design-of-an-eye-in-tikz/94087
%%% source : https://tex.stackexchange.com/questions/58702/creating-gears-in-tikz
\begin{frame}
\frametitle{\ref{prelim}. Opacité binaire}
\textbf{Éléments :}
\begin{itemize}
  \item \visible<2->{Secret $\varphi$} \visible<5->{{\color{red} ~ -- langage régulier de $Exec(S)$}}
  \item \visible<2->{Système $S$} \visible<5->{{\color{red} ~ -- Système de transitions}}
  \item \visible<3->{Attaquant passif extérieur $\mathcal{O}$} \visible<5->{{\color{red} ~-- projection naturelle $\Sigma\rightarrow\Sigma$}}
\end{itemize}

\def\eyepath{(-3,0) .. controls (-2,1.8) and (2,2.2) .. (2.7,0) .. controls (2,-1.2) and (-2,-1.4) .. (-3,0)--cycle;}


\begin{figure}
% \only<1>{
% \begin{tikzpicture}
% \node (A) [] {$\varphi$};
% \end{tikzpicture}}
\visible<2->{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
\node (A) [] {$\varphi$};
\node (B) [right of=A,align=left] {système $S$\\exécution $\sigma$};
\path (A) edge [swap] node {~} (B);
\end{tikzpicture}
\begin{tikzpicture}[scale=0.2]
\fill[even odd rule] \gear{10}{2}{2.4}{10}{2}{1};
\end{tikzpicture}}
\visible<3->{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
\node (A) [] {};
\node (B) [right of=A,align=right] {observateur\\passif $\mathcal{O}$};
\path (A) edge [swap]node {$\mathcal{O}(\sigma)$} (B);
\end{tikzpicture}
\begin{tikzpicture}[scale=0.25]%%% EYE
\clip\eyepath;
\filldraw[color=orange!50!black] (-.2,.2) circle (1.5);
\fill[color=black] (-.2,.2) circle (0.7);
\fill[color=white] (.3,.5) circle (0.2);
\draw[very thick]\eyepath;
\end{tikzpicture}}
\uncover<2->{\caption{Schéma de principe de l'opacité}}
\end{figure}
\visible<4->{
\centering
{\bf Question :} L'attaquant peut-il déduire la valeur de vérité du secret en observant uniquement les traces d'exécution partielles?
}
~
\end{frame} 


\begin{frame}
\frametitle{\ref{prelim}. Opacité binaire -- Formalisation}
\visible<1->{Y a-t-il une exécution $\sigma$ du secret $\varphi$ qui n'est jamais observée comme une exécution non-secrète ?}

\visible<3->{
\begin{beamerboxesrounded}[upper=titre,lower=texte]{Définition}
Soient un système de transitions $S$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur $\mathcal{O}$. 

$\varphi$ est opaque dans $\mathcal{A}$ relativement à $\mathcal{O}$ ssi 
\[ \mathcal{O}(\varphi) \subseteq \mathcal{O}(L\setminus\varphi).\]
 \end{beamerboxesrounded}
}

\invisible<1>{
\begin{figure}[h]
\centering
\subfloat[Secret opaque]{
\includegraphics[height=80px]{schema-opaque2}
}
~
\subfloat[Secret non-opaque]{
\includegraphics[height=80px]{schema-nonopaque}
}
\end{figure}
}
~
\end{frame} 


\begin{frame}
\frametitle{\ref{prelim}. Opacité binaire -- Exemple}

\visible<1->{
  \begin{itemize}
    \item Alphabet : $\Sigma=\{h,l_1,l_2\}$
    \item Secret : $\varphi=\Sigma^* h \Sigma^\omega$ -- les exécutions possédant un $h$
    \item Observateur : $\mathcal{O}(l_1) = l_1$, $\mathcal{O}(l_2)=l_2$ et $\mathcal{O}(h)=\varepsilon$
  \end{itemize}
}


\begin{figure}[h]
\uncover<2->{
\centering
\subfloat[Secret opaque]{
\scriptsize{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$l_1$};
    % \node (ph) [right of=A] {};
    \node[state]         (B) [right of=A]        {$h$};
    \node[state]         (C) [right of=B]        {$l_2$};
    % \node (E) [right of=ph] {};

    \path (A) edge node {} (B)
              edge [out=30,in=150] node {} (C)
          (B) edge node {} (C)
          (C) edge [out=30,in=330,looseness=7] node {} (C);

\end{tikzpicture}}}}
~
\uncover<3->{
\subfloat[Secret non-opaque]{
\scriptsize{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$l_1$};
    % \node (ph) [right of=A] {};
    \node[state]         (B) [right of=A]        {$h$};
    \node[state] (D) [above of=B] {$l_1$};
    \node[state]         (C) [right of=B]        {$l_2$};
    % \node (E) [right of=ph] {};

    \path (A) edge node {} (B)
              edge node {} (D)
              % edge [out=30,in=150] node {} (C)
          (B) edge node {} (C)
              edge node {} (D)
          (C) edge [out=30,in=330,looseness=7] node {} (C)
          (D) edge [out=30,in=330,looseness=7] node {} (D);

\end{tikzpicture}}}
}
\end{figure}
~
\end{frame}



\begin{frame}
\frametitle{\ref{prelim}. Opacité libérale}
\centering
{\bf But :} Mesurer le degré de {\bf non-opacité}
\visible<2->{$\Rightarrow$ on probabilise le système}

\uncover<4->{
    \begin{beamerboxesrounded}[upper=titre,lower=texte]{Définition}
Soient une implémentation $S$ dont le langage des exécutions est $L$, un secret $\omega$-régulier $\varphi\subseteq L$ et un observateur $\mathcal{O}$. L'\emph{opacité libérale de $\varphi$ dans $\mathcal{A}$ relativement à $\mathcal{O}$} est la grandeur~: \phantom{dont le langage des exécutions est}\includegraphics[height=1em]{ensemble-v}
\[ PO_l(S,\varphi,\mathcal{O}) = \prob \big(\varphi \cap \overline{\mathcal{O}^{-1}(\mathcal{O}(L\setminus\varphi))}\big).\]
 \end{beamerboxesrounded}
}

\begin{columns}[c]
\begin{column}{5cm}
\uncover<3->{
\scriptsize{
\begin{figure}
  \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$l_1$};
    % \node (ph) [right of=A] {};
    \node[state]         (B) [right of=A]        {$h$};
    \node[state] (D) [above of=B] {$l_1$};
    \node[state]         (C) [right of=B]        {$l_2$};
    % \node (E) [right of=ph] {};

    \path (A) edge node {0.2} (B)
              edge node {0.8} (D)
              % edge [out=30,in=150] node {} (C)
          (B) edge node {0.5} (C)
              edge node {0.5} (D)
          (C) edge [out=30,in=330,looseness=7] node {1} (C)
          (D) edge [out=30,in=330,looseness=7] node {1} (D);
\end{tikzpicture}
\caption{Système probabiliste}
\end{figure}}}
\end{column}

\begin{column}{5cm}
\uncover<5->{
\begin{table}
  \begin{tabular}{|c|c|c|c|}
\hline
$\sigma$ & $\prob(\sigma)$ & secret & $\mathcal{O}(\sigma)$\\
\hline
$l_1^\omega$ & 0.8 & non & $l_1^\omega$\\
\hline
$l_1hl_1^\omega$&0.1&oui&$l_1^\omega$\\
\hline
$l_1hl_2^\omega$&0.1&oui&$l_1l_2^\omega$\\
\hline
  \end{tabular}
  \caption{Calcul d'opacité libérale}
  \end{table}
}
\end{column}
\end{columns}


\uncover<6->{
  L'opacité libérale du secret dans ce système de transitions est de {\bf 0.1.}
}
~
\end{frame} 

\begin{frame}
\frametitle{\ref{prelim}. Propriétés de l'opacité libérale}
\begin{itemize}
  \item La mesure croît avec le degré de non-opacité.
  \item Le secret est opaque ssi la mesure de l'opacité libérale est nulle.
\end{itemize}
~
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Spécification et raffinement}\label{spec}

\begin{frame}
\frametitle{\ref{spec}. Chaîne de Markov discrète à intervalles (IDTMC)}
\begin{columns}[t]

\begin{column}{7.5cm}
% \uncover<2>{
\begin{figure}[h]
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    \node[initial,state,label={$q_0$}] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state,label={$q_1$}]         (B) [above of=ph]        {$b$};
    \node[state,label={$q_2$}]         (C) [below of=ph]        {$c$};

    \path (A) edge node {[0.1,1]} (B)
              edge node[swap] {[0,1]} (C)
          (B) edge [out=30,in=330,looseness=7] node {[1,1]} (B)
          (C) edge [out=30,in=330,looseness=7] node {[1,1]} (C);

\end{tikzpicture}
\caption{Exemple d'IDTMC}
\end{figure}
% }
\end{column}

\begin{column}{5cm}
\begin{itemize}
  \item Modèle de spécification
  \item Non-déterministe
  \item Implémentation = PTS
\begin{itemize}
  \item But : résoudre le non-déterminisme
  \item Moyen : Ordonnanceur
\end{itemize}
\end{itemize}
\end{column}

\end{columns}
~
\end{frame} 




\begin{frame}
\frametitle{\ref{spec}. Ordonnancement}
Un ordonnanceur $A$ d'une spécification $\Spec$ ($A\in Sched(\Spec)$) :
\begin{itemize}
  \item est un adversaire omniscient du modèle
  \item réalise le choix de la distribution
  \item implémente le modèle en un PTS
\end{itemize}


\begin{columns}[c]
\begin{column}{6cm}
\begin{figure}[h]
% \uncover<2->{
\centering
\uncover<2->{
\subfloat[IDTMC $\Spec$]{
\tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$a$};
    \node (ph) [right of=A] {};
    \node[state]         (B) [above right of=A]        {$b$};
    \node[state]         (C) [below right of=A]        {$c$};
    \node (E) [right of=ph] {};

    \path (A) edge [bend left] node {[0.1,1]} (B)
              edge [bend right] node[swap] {[0,1]} (C)
          (C) edge [bend right] node[swap] {1} (A)
          (B) edge [bend left] node  {1} (A);

\end{tikzpicture}}}
}
% \uncover<3->{

\uncover<3->{\subfloat[Mémoire d'ordonnanceur $A$ de taille 2]{
\tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

      \node[initial,state,draw=red] (A)                     {$1$};
    \node[state,draw=blue] (B) [right of=A] {$2$};
    
    \path[->] (A) edge [bend left] node {$b$} (B)
      (B) edge [bend left] node {$c$} (A)
      edge [out=120,in=60,looseness=7] node {$a,b$} (B)
    (A) edge [out=120,in=60, looseness=7]  node {$a,c$} (A);
    
\end{tikzpicture}}}}
\end{figure}
\end{column}

\begin{column}{6cm}

\uncover<4>{
\begin{figure}
\small{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

     \node[initial,state,draw=red] (A)                     {$a$};
    % \node (ph) [right of=A] {};
    \node[state,draw=red]         (B) [above right of=A]        {$b$};
    \node[state,draw=red]         (C) [below right of=A]        {$c$};
    \node[state,draw=blue] (A1) [right of=B] {$a$};
    \node[state,draw=blue] (B1) [above right of=A1] {$b$};
    \node[state,draw=blue] (C1) [below right of=A1] {$c$};
    % \node (E) [right of=ph] {};

    \path (A) edge node {0.1} (B)
              edge [bend right] node[swap] {0.9} (C)
          (C) edge [bend right] node[swap] {1} (A)
          (B) edge node  {1} (A1)
          (A1) edge[bend left] node {0.5} (B1)
              edge node {0.5} (C1)
          (B1) edge[bend left] node {1} (A1)
          (C1) edge node {1} (A);
    
\end{tikzpicture}}
\caption{Un ordonnancement $\Spec(A)$}
\end{figure}}
\end{column}
\end{columns}
~
\end{frame} 


\begin{frame}
\frametitle{\ref{spec}. Raffinement}
~
\end{frame} 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Calcul de l'opacité dans les spécifications}\label{opacite}

\begin{frame}
\frametitle{\ref{opacite}. Opacité libérale d'une spécification}
Dans une spécification, on considère le {\bf pire cas possible}.
\vfill
   \begin{beamerboxesrounded}[upper=titre,lower=texte]{Définition}
Soient une spécification IDTMC $\Spec$, un secret $\omega$-régulier $\varphi$ et un observateur rationnel $\mathcal{O}$. L'\emph{opacité libérale de $\varphi$ dans $\Spec$ relativement à $\mathcal{O}$} est la grandeur~:
% \[ PO_l(\Spec,\varphi,\mathcal{O}) = \sup_{\mathcal{A}\in \sat(\Spec)} PO_l(\mathcal{A},\varphi,\mathcal{O}).\]
% Si on restreint le calcul à l'ensemble des implémentations ordonnancées, on définit~:
\[ \polO(\Spec,\varphi,\mathcal{O}) = \sup_{A\in Sched(\Spec)} PO_l(\Spec(A),\varphi,\mathcal{O}).\]
\end{beamerboxesrounded}
~
\end{frame}

\begin{frame}
\frametitle{\ref{opacite}. Modalité des spécifications}
Une spécification {\bf modale} possède au moins une transition dont la probabilité peut {\bf s'annuler}.

\uncover<3->{
\Large{{\color{red} Problème !} $\Rightarrow$ Il n'y a pas unicité du langage d'une spécification !}
}

\uncover<2->{
\begin{figure}
\subfloat[Spécification modale]{
\tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$a$};
    % \node (ph) [right of=A] {};
    \node[state]         (B) [right of=A]        {$b$};
    \node[state]         (C) [right of=B]        {$c$};
    % \node (E) [right of=ph] {};

    \path (A) edge node [swap]{[0.1,1]} (B)
              edge [bend left] node {{\color{red} [0,1]}} (C)
          (C) edge [out=-30,in=30,looseness=7] node [swap]{1} (C)
          (B) edge  node [swap] {1} (C);
\end{tikzpicture}}}

\subfloat[Premier ordonnancement]{
  \tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$a$};
    % \node (ph) [right of=A] {};
    \node[state]         (B) [right of=A]        {$b$};
    \node[state]         (C) [right of=B]        {$c$};
    % \node (E) [right of=ph] {};

    \path (A) edge node [swap]{1} (B)
              % edge [bend left] node {{\color{red} [0,1]}} (C)
          (C) edge [out=-30,in=30,looseness=7] node [swap]{1} (C)
          (B) edge  node [swap] {1} (C);

\end{tikzpicture}
  }
}

\subfloat[Second ordonnancement]{
  \tiny{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=5em,semithick]
      \tikzstyle{every state}+=[fill=white,text=black]

    \node[initial,state] (A)                     {$a$};
    % \node (ph) [right of=A] {};
    \node[state]         (B) [right of=A]        {$b$};
    \node[state]         (C) [right of=B]        {$c$};
    % \node (E) [right of=ph] {};

    \path (A) edge node [swap]{0.9} (B)
              edge [bend left] node {0.1} (C)
          (C) edge [out=-30,in=30,looseness=7] node [swap]{1} (C)
          (B) edge  node [swap] {1} (C);

\end{tikzpicture}
  }
}
\end{figure}}
~
\end{frame}






\begin{frame}
\frametitle{\ref{opacite}. Cas des spécifications non-modales}
\begin{beamerboxesrounded}[upper=titre,lower=texte]{Théorème}
Soient une IDTMC $\Spec$ \emph{non-modale}, un secret $\omega$-régulier $\varphi$ et un observateur $\mathcal{O}$. Alors l'opacité libérale de $\varphi$ dans $\Spec$ est mesurable.
\end{beamerboxesrounded}
% \end{frame} 



% \begin{frame}
% \frametitle{\ref{opacite}. Algorithme -- cas non-modal}
\textbf{IDÉE : Transformer le problème en calcul de probabilité de langage dans un MDP}

\begin{itemize}
  \item<2-> {\bf Entrée :} secret $\varphi$, spécification $\Spec$ non-modale, observateur $\mathcal{O}$
  \item<2-> {\bf Sortie :} opacité libérale
  \item<3-> {\bf Étape 1 :} exprimer le langage $\mathcal{V}$ des exécutions brisant l'opacité sous forme d'automate
  \item<4-> {\bf Étape 2 :} extraire la sous-spécification de $\Spec$ dont le langage est $\mathcal{V}$
  \item<5-> {\bf Étape 3 :} transformer la sous-spécification en MDP
  \item<6-> {\bf Étape 4 :} calculer la probabilité maximale de $\mathcal{V}$ dans le MDP
\end{itemize}
~
\end{frame} 



\begin{frame}
\frametitle{\ref{opacite}. Cas des spécifications modales}
\begin{beamerboxesrounded}[upper=titre,lower=texte]{Théorème}
Soient une IDTMC $\Spec$ \emph{modale}, un secret $\omega$-régulier $\varphi$ et un observateur $\mathcal{O}$. Alors on peut approcher par la borne inférieure la mesure de l'opacité libérale.
\end{beamerboxesrounded}

\textbf{IDÉE : S'affranchir de la modalité des transitions et se ramener au cas non-modal}

\begin{itemize}
  \item<2-> {\bf Entrée :} secret $\varphi$, spécification $\Spec$ modale, observateur $\mathcal{O}$, taille de mémoire $n$
  \item<2-> {\bf Sortie :} minorant de l'opacité libérale
  \item<3-> {\bf Étape 1 :} pour chaque type de mémoire d'ordonnanceur de taille $n$, 
  \begin{itemize}
    \item<4-> {\bf Étape 1.1 :} déplier l'ordonnancement sans choisir les distributions
    \item<5-> {\bf Étape 1.2 :} déterminer l'ensemble des transitions modales $\mathcal{E}_m$
    \item<6-> {\bf Étape 1.3 :} pour chaque sous-ensemble $\mathcal{E}\subseteq\mathcal{E}_m$,
    \begin{itemize}
      \item<7-> {\bf Étape 1.3.1 :} annuler les transitions de $\mathcal{E}$ et ouvrir les intervalles des autres transitions modales
      \item<8-> {\bf Étape 1.3.2 :} appliquer l'algorithme précédent sur la spécification non-modale
    \end{itemize}
  \end{itemize}
\end{itemize}
~
\end{frame} 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Préservation de l'opacité}\label{preser}

\begin{frame}
\frametitle{\ref{preser}. Préservation de l'opacité}
   \begin{beamerboxesrounded}[upper=titre,lower=texte]{Théorème}
Soient deux IDTMC $\Spec_1$ et $\Spec_2$ telles que $\Spec_1$ raffine faiblement $\Spec_2$, un secret $\omega$-régulier $\varphi$ et un observateur $\mathcal{O}$. 

Alors $\polO(\Spec_1,\varphi,\mathcal{O})\leq\polO(\Spec_2,\varphi,\mathcal{O})$.
\end{beamerboxesrounded}
~
\end{frame}



\begin{frame}
\frametitle{\ref{preser}. Étude de cas}
~
\end{frame} 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}

\begin{frame}
\frametitle{Bilan des contributions}
Contributions principales :
\begin{itemize}
  \item Extension du calcul d'opacité au cas des spécifications modales
  \item Extension du théorème de préservation au cas du raffinement \textbf{faible}
  \item Étude de cas
\end{itemize}

Contributions secondaires :
\begin{itemize}
  \item Extension de la quasi-opacité uniforme aux IDTMC
  \item Exploration de l'opacité restrictive
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Questions ouvertes}
\begin{itemize}
  \item Calcul exact dans le cas des spécifications modales ?
  \item Préservation de l'opacité par raffinement complet ?
  \item Extension des résultats à d'autres méthodes de spécification ?
\end{itemize}
\end{frame}


\end{document}