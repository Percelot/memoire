\select@language {french}
\contentsline {compteur}{D\IeC {\'E}DICACE}{iii}{chapter*.1}
\contentsline {compteur}{REMERCIEMENTS}{iv}{chapter*.2}
\contentsline {compteur}{R\IeC {\'E}SUM\IeC {\'E}}{v}{chapter*.3}
\contentsline {compteur}{ABSTRACT}{vii}{chapter*.4}
\select@language {english}
\select@language {french}
\contentsline {compteur}{TABLE DES MATI\IeC {\`E}RES}{ix}{chapter*.5}
\contentsline {compteur}{LISTE DES TABLEAUX}{xii}{chapter*.6}
\contentsline {compteur}{LISTE DES FIGURES}{xiii}{chapter*.7}
\contentsline {compteur}{LISTE DES SIGLES ET ABR\IeC {\'E}VIATIONS}{xiv}{chapter*.8}
\contentsline {chapter}{\numberline {1}INTRODUCTION}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}\IeC {\'E}l\IeC {\'e}ments de la probl\IeC {\'e}matique}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Exemple de motivation}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Objectifs de recherche}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}M\IeC {\'e}thodologie}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Plan du m\IeC {\'e}moire}{6}{section.1.5}
\contentsline {chapter}{\numberline {2}REVUE DE LITT\IeC {\'E}RATURE}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Raffinement et sp\IeC {\'e}cification}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Le cas des syst\IeC {\`e}mes s\IeC {\'e}curis\IeC {\'e}s}{8}{section.2.2}
\contentsline {chapter}{\numberline {3}PR\IeC {\'E}LIMINAIRES}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Mod\IeC {\'e}lisation}{12}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Langages et automates}{12}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Les syst\IeC {\`e}mes de transitions}{14}{subsection.3.1.2}
\contentsline {paragraph}{Probabilisation}{15}{section*.9}
\contentsline {subsection}{\numberline {3.1.3}Un mod\IeC {\`e}le de sp\IeC {\'e}cification}{17}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}V\IeC {\'e}rification de l'opacit\IeC {\'e}}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Une opacit\IeC {\'e} binaire}{19}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}L'opacit\IeC {\'e} probabiliste}{20}{subsection.3.2.2}
\contentsline {subsubsection}{L'opacit\IeC {\'e} probabiliste lib\IeC {\'e}rale}{20}{section*.10}
\contentsline {subsubsection}{D'autres formes d'opacit\IeC {\'e}}{21}{section*.11}
\contentsline {paragraph}{Opacit\IeC {\'e} restrictive}{21}{section*.12}
\contentsline {paragraph}{Un mod\IeC {\`e}le alternatif}{22}{section*.13}
\contentsline {paragraph}{En r\IeC {\'e}sum\IeC {\'e}}{24}{section*.14}
\contentsline {chapter}{\numberline {4}TH\IeC {\'E}ORIE DES SP\IeC {\'E}CIFICATIONS}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}IDTMC}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Raffinement fort et faible}{27}{section.4.2}
\contentsline {section}{\numberline {4.3}Impl\IeC {\'e}mentation et raffinement complet}{28}{section.4.3}
\contentsline {section}{\numberline {4.4}Langages dans les \ac {IDTMC}}{31}{section.4.4}
\contentsline {section}{\numberline {4.5}Ordonnancement}{33}{section.4.5}
\contentsline {section}{\numberline {4.6}Extension de l'opacit\IeC {\'e} lib\IeC {\'e}rale aux IDTMC}{37}{section.4.6}
\contentsline {chapter}{\numberline {5}V\IeC {\'E}RIFICATION DE L'OPACIT\IeC {\'E}}{39}{chapter.5}
\contentsline {section}{\numberline {5.1}Notions pr\IeC {\'e}liminaires}{39}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Synchronisation entre un DPA et une IDTMC}{39}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}\acf {BFS}}{41}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Calcul d'un MDP \IeC {\`a} partir d'une IDTMC}{43}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Calcul de l'opacit\IeC {\'e} lib\IeC {\'e}rale dans le cas des IDTMC non-modales}{44}{section.5.2}
\contentsline {paragraph}{\IeC {\'E}tape 1.}{45}{section*.15}
\contentsline {paragraph}{\IeC {\'E}tape 2.}{46}{section*.16}
\contentsline {paragraph}{\IeC {\'E}tape 3.}{46}{section*.17}
\contentsline {section}{\numberline {5.3}Une approximation du cas g\IeC {\'e}n\IeC {\'e}ral}{46}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}D\IeC {\'e}termination des transitions modales}{47}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}\IeC {\'E}limination de certaines transitions modales}{48}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}D\IeC {\'e}pliage de l'ordonnancement}{50}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Approximation du calcul de l'opacit\IeC {\'e} lib\IeC {\'e}rale dans le cas des IDTMC modales}{50}{subsection.5.3.4}
\contentsline {section}{\numberline {5.4}Pr\IeC {\'e}servation de l'opacit\IeC {\'e} lib\IeC {\'e}rale par raffinement}{54}{section.5.4}
\contentsline {paragraph}{Quelques notations \IeC {\`a} propos des pr\IeC {\'e}fixes finis d'ex\IeC {\'e}cutions}{55}{section*.18}
\contentsline {section}{\numberline {5.5}Cas des autres opacit\IeC {\'e}s}{57}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Quasi-opacit\IeC {\'e} uniforme}{57}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Opacit\IeC {\'e} restrictive}{59}{subsection.5.5.2}
\contentsline {chapter}{\numberline {6}\IeC {\'E}TUDE DE CAS}{60}{chapter.6}
\contentsline {section}{\numberline {6.1}Description de l'\IeC {\'e}tude de cas}{60}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Mod\IeC {\'e}lisation du syst\IeC {\`e}me}{60}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Requis de s\IeC {\'e}curit\IeC {\'e}}{62}{subsection.6.1.2}
\contentsline {section}{\numberline {6.2}Calcul de l'opacit\IeC {\'e} binaire}{63}{section.6.2}
\contentsline {section}{\numberline {6.3}Opacit\IeC {\'e} lib\IeC {\'e}rale}{64}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Application du th\IeC {\'e}or\IeC {\`e}me \ref {thm:calculNM}}{65}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Bilan du calcul}{68}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Un autre exemple}{68}{subsection.6.3.3}
\contentsline {section}{\numberline {6.4}Un raffinement}{71}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Calcul d'opacit\IeC {\'e} lib\IeC {\'e}rale}{73}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Calcul d'opacit\IeC {\'e} restrictive}{74}{subsection.6.4.2}
\contentsline {chapter}{\numberline {7}CONCLUSION}{78}{chapter.7}
\contentsline {section}{\numberline {7.1}Synth\IeC {\`e}se des travaux}{78}{section.7.1}
\contentsline {section}{\numberline {7.2}Limitations de la solution propos\IeC {\'e}e}{79}{section.7.2}
\contentsline {section}{\numberline {7.3}Am\IeC {\'e}liorations futures}{80}{section.7.3}
\contentsline {compteur}{R\IeC {\'E}F\IeC {\'E}RENCES}{81}{chapter*.19}
