#!/bin/bash

echo '\begin{longtable}{lp{5in}}' >> 4-Sigles_Abrev.tex

grep acro{ 4-Sigles_Abrev.tex > temp
sed -r -i.bak 's/^.{8}//' temp
sed -r -i.bak 's/.$//' temp
sed -r -i.bak 's/\{//' temp

while IFS=$'\}\{': read acro full
do
echo ${acro}' & '${full}'\\' >> 4-Sigles_Abrev.tex
done < temp


echo '\end{longtable}' >> 4-Sigles_Abrev.tex

rm temp

