\Chapter{CONCLUSION}\label{sec:Conclusion}


%%
%%  SYNTHESE DES TRAVAUX
%%
\section{Synthèse des travaux}

Le but de ce mémoire consistait à étudier la formalisation du raffinement de systèmes sécurisés. En règle générale, il est connu que cette problématique aboutit à un résultat négatif~: en effet, le raffinement d'un système sécurisé peut briser la sécurité et le rendre vulnérable pour certaines propriétés. Cependant, ce résultat n'est pas absolu, et notre objectif était de représenter un cadre formel dans lequel raffiner un système sécurisé préserverait la sécurité. 

Dans cette optique, nous avons tout d'abord défini le cadre formel de recherche, en choisissant comme modèle de spécification l'\ac{IDTMC}, couplé au \ac{PTS} en tant que modèle d'implémentation. Ces deux modèles utilisant les hypothèses markoviennes permettent la représentation de nombreux systèmes réels, tout en appliquant des méthodes quantitatives, nécessaire à la modélisation de propriétés de sécurité. En parallèle, nous avons choisi l'opacité comme propriété formelle de sécurité, possédant l'avantage d'être connue dans le cadre des \ac{PTS} et étant calculable en un temps fini. De plus, l'opacité est liée aux propriétés de flux d'information, qui sont au cœur d'importantes problématiques sur la confidentialité des données et des échanges~: elles représentent l'information que peut déduire un agent extérieur en observant un système. 

Afin de justifier l'intérêt du choix de ce cadre, nous avons commencé par définir l'extension de l'opacité au domaine des \ac{IDTMC}, puis nous avons prouvé que cette grandeur était calculable ou approchable en un temps fini. De fait, nous avons expliqué la méthode permettant de calculer l'opacité libérale dans une \ac{IDTMC} non-modale, c'est-à-dire une spécification dont toutes les transitions sont nécessairement ordonnancées. Ce calcul, revenant à l'optimisation d'un système linéaire dans un ensemble fermé borné, se réalise en un temps doublement exponentiel. De plus, en étudiant le cas général, nous avons construit les algorithmes permettant de nous ramener à des calculs d'opacité dans le cas des spécifications non-modales. Ceux-ci sont fondés sur le fait que nous listons de manière formelle tous les ordonnancements à mémoire finie de l'\ac{IDTMC}, afin d'en sortir le cas le moins sécurisé. Ainsi, nous avons pu affirmer que la valeur de l'opacité libérale dans le cas général pouvait être approchée en un temps doublement exponentiel, ce qui constitue notre première contribution originale en étendant le résultat précédent, limité à une partie des spécifications.

Consécutivement, nous avons résolu notre problématique en énonçant le théorème suivant~: l'opacité libérale d'une spécification \ac{IDTMC} est, dans le pire des cas, conservée lorsque la spécification est raffinée faiblement. Ce résultat s'appuie sur l'étude de la relation de raffinement d'une part, et la relation d'ordonnancement d'autre part. Il constitue notre seconde contribution originale, en généralisant le résultat déjà connue de la préservation de l'opacité par raffinement fort uniquement.

Enfin, nous avons illustré tous ces résultats sur un exemple d'application. Nous avons considéré un système de contrôle d'accès à une base de données confidentielles, que nous avons modélisé conformément à notre cadre d'études. Nous avons calculé que la première spécification n'était pas opaque, en appliquant les algorithmes expliqués précédemment. Nous avons alors défini un raffinement du système, et dans la même optique, nous avons assuré l'opacité, ce qui a illustré le résultat de préservation de la sécurité.

%% TODO: %%%%%%%%%%%%%%%%%% Ajouter article de journal !?!

%% Définition du cadre du travail
%% Extension de l'opacité aux spécifications
%% Preuve des théorèmes + algorithmes

%%
%%  LIMITATIONS
%%
\section{Limitations de la solution proposée}\label{sec:Limitations}

Bien qu'il constitue une avancée dans la théorie, ce travail de recherche comporte certaines limitations que nous n'avons pu franchir. 

Tout d'abord, le théorème de calcul de l'opacité libérale dans le cas général n'aboutit pas à la preuve que la valeur exacte est calculable en temps fini. En effet, puisque l'algorithme consiste à énumérer les cas pour les différents ordonnanceurs à mémoire finie, et puisqu'il y a théoriquement une infinité de ces ordonnanceurs -- sans compter les ordonnanceurs à mémoire quelconque -- il est \emph{a priori} impossible de réaliser le calcul exact en un temps fini~: la question reste ouverte.

En second lieu, nous avons prouvé que l'opacité libérale était préservée lors d'un raffinement faible, ce qui implique qu'elle est préservée lors d'un raffinement fort. Pour autant, rien n'est prouvé pour le raffinement complet. Cette question est encore ouverte.

Enfin, la plupart de l'étude a été réalisée en prenant en compte l'opacité libérale. La question quant à sa grandeur duale, l'opacité restrictive, est encore en suspens. Or cette opacité est justement intéressante pour distinguer des systèmes dont le secret est effectivement opaque, à l'inverse de l'opacité libérale, qui détermine le degré de non-opacité du secret dans un système. La difficulté réside ici dans le fait que l'opacité restrictive dans un \ac{PTS} n'est pas la probabilité d'un ensemble régulier mais le résultat d'une moyenne harmonique de probabilités particulières. 

%%
%%  AMELIORATIONS FUTURES
%%
\section{Améliorations futures}

Cette recherche est le fruit de l'étude d'un cadre particulier dans la problématique du raffinement de systèmes sécurisés. Les travaux futurs consistent donc à continuer dans cette voie en essayant de généraliser les résultats obtenus. 

La première généralisation consiste à vérifier si les théorèmes sont toujours valables si l'on modifie la nature de la propriété. Notamment, que se passe-t-il lorsque l'on remplace l'opacité libérale par une autre \ac{RIFP} \citep[][]{Berard2014-rifp} ? Si on parvient à prouver la généralisation, il sera possible de considérer un grand nombre de propriétés formelles de sécurité. En effet, il est possible de prouver que les Prédicats Basiques de Sécurité (\acs{BSP}) \citep[][]{Mantel2000,Mantel2001} -- introduites par Mantel et qui constituent les briques élémentaires de toutes les propriétés de sécurité -- peuvent être vues comme des \acs{RIFP}, en les calculant sous le sens libéral. Ces deux résultats combinés sont autant de poinsts justifiant l'intérêt de ce formalisme.

La seconde généralisation se trouve du côté du choix de modélisation. Le choix de l'\ac{IDTMC} permet un formalisme contraint qui facilite les résultats. Serait-il possible d'étendre le tout à des objets plus généraux, tels que les Chaînes de Markov Contraintes (\acs{CMC}) ? Dans ce formalisme, les probabilités de transitions ne sont plus restreintes par des intervalles, mais par des relations de contraintes quelconques. Ainsi, les \ac{IDTMC} sont des \acs{CMC} particulières. 

% La suite du travail consiste finalement à étendre le domaine d'application de ces résultats.
%% Raffinement complet ? Y a-t-il un moyen de vérifier le théorème ? (énoncer le théorème avec ce raffinement)
%% Étendre à toutes les RIFP, de manière immédiate ==> on peut ainsi vérifier que c'est valable pour toutes les BSP de Mantel
%% Étendre à d'autres modèles de spécifications ==> exemple de la CMC linéaire, où les probabilités ne sont plus restreintes par des intervalles mais par des contraintes linéaires
%% Étudier un protocole existant, le modéliser et essayer de le raffiner (???)
Au-delà de ces extensions du domaine d'application des résultats, il serait intéressant d'explorer davantage le calcul de l'opacité restrictive dans les IDTMC. L'aspect non-linéaire de cette grandeur pose problème, mais peut-être existe-t-il un moyen de linéariser les fonctions afin d'avoir une approximation de la valeur. Il faudrait pour cela suivre des pistes d'optimisation de fonctions.

Enfin, nos résultats sont pour le moment dans un cadre très théorique et peu applicatif. Le logiciel de modélisation et vérification PRISM est actuellement capable de calculer l'opacité libérale d'un PTS ; il serait intéressant d'explorer la possibilité de modéliser les IDTMC sur ce logiciel. Si cela s'avère possible, il serait alors envisageable de calculer l'opacité libérale d'une IDTMC, la difficulté étant alors de se limiter aux ordonnancements.