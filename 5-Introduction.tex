%   Dans l'introduction, on présente le problème étudié et les buts
% poursuivis. L'introduction permet de faire connaître le cadre de la
% recherche et d'en préciser le domaine d'application. Elle fournit
% les précisions nécessaires en ce qui concerne le contexte de
% réalisation de la recherche, l'approche envisagée, l'évolution de
% la réalisation. En fait, l'introduction présente au lecteur ce
% qu'il doit savoir pour comprendre la recherche et en connaître la
% portée.
\Chapter{INTRODUCTION}\label{sec:Introduction}  % 10-12 lignes pour introduire le sujet.
Que ce soit pour comprendre le fonctionnement d'un système ou pour concevoir un produit industriel, il est courant de faire appel à des méthodes de modélisation formelle. Cela consiste à dégager dans un premier temps les caractéristiques principales, en écartant les concepts secondaires, \emph{a priori} inutiles pour comprendre le fonctionnement du système. On obtient alors un modèle théorique qui décrit le comportement du système, mais qui ne le représente pas exactement. 
Afin de minimiser l'écart entre le système et le modèle, on a recours, lors de la gestion de projet, au processus de raffinement~: le modèle est constamment amélioré, et les caractéristiques fonctionnelles initialement écartées sont progressivement étudiées et ajoutées. Ceci est une manière de passer d'une idée théorique à un produit réel implémentant cette idée. 
% Une problématique supplémentaire se pose alors~: il est nécessaire de s'assurer qu'aucune étape du raffinement ne brise le fonctionnement du système. Autrement dit, il est nécessaire de s'assurer, pour chaque étape, que le nouveau modèle vérifie au moins les mêmes propriétés que l'ancien.


%%
%% ELEMENTS DE LA PROBLEMATIQUE
%%
\section{Éléments de la problématique}  % environ 3 pages

En génie logiciel et informatique, les méthodes formelles sont généralement utilisées pour modéliser, concevoir et vérifier des systèmes critiques, notamment dans des domaines tels que le ferroviaire ou l'avionique, où les risques humains demandent une résistance parfaite aux failles. Le raffinement est une part importante de ces méthodes, puisque la conception formelle d'un système passe par une succession d'étapes ajoutant régulièrement des fonctionnalités. Il est alors nécessaire de s'assurer qu'aucune étape du raffinement ne brise le fonctionnement du système. Autrement dit, il est nécessaire de s'assurer, pour chaque étape, que le nouveau modèle vérifie au moins les mêmes propriétés que l'ancien. On parle notamment de raffinement de requis fonctionnels -- par exemple, l'absence de blocage, ou la garantie que le train s'arrête lors d'un feu rouge. 

Ces requis fonctionnels ne sont cependant pas les seuls requis que peut comporter le cahier des charges d'un système. On distingue également les requis non-fonctionnels. Par exemple, si l'on considère un fournisseur de services d'infonuage, l'un des requis non-fonctionnels classiques est l'obligation de fournir une qualité de service quasiment parfaite aux clients. Il est courant alors de quantifier le temps maximal autorisé de baisse de service durant une période donnée, ou bien la probabilité qu'une panne se déroule. De façon naturelle, la modélisation de ces requis se réalise donc grâce à des méthodes quantitatives. 

% La vérification d'une propriété par un système est le second aspect important des méthodes formelles. On distingue alors les méthodes qualitatives des méthodes quantitatives. Dans le premier cas, le but est d'affirmer si toutes les exécutions du modèles valident la propriété donnée. Ainsi, la réponse des méthodes qualitatives est binaire. Cet aspect permet de discriminer les systèmes en deux groupes, mais s'avère relativement faible. En effet, si un modèle ne vérifie pas une propriété donnée, est-ce que cela veut dire qu'aucune de ses exécutions ne la vérifient, ou bien seulement certaines d'entre elles ?

% Dans le but d'affiner la réponse, on fait appel aux méthodes quantitatives, dont le but est de déterminer à quel point un système vérifie ou non une propriété donnée. Pour cela, il est nécessaire d'introduire une grandeur numérique dans le modèle. L'une des méthodes généralement utilisées est l'introduction de probabilité. 
% Ainsi, on peut représenter un système par un arbre de probabilité. Par exemple, dans la figure \ref{exProba}, l'exécution $ab$ a 80\% de chance de se produire. 

% \begin{figure}[h]
% \centering
% \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3.5cm,semithick]
%       \tikzstyle{every state}=[fill=white,text=black]

%     \node[initial,state,label=right:{$q_0$}] (A)                     {$a$};
%     \node[state,label={$q_1$}]         (B) [above right of=A]        {$b$};
%     \node[state,label=below:{$q_2$}]         (C) [below right of=A]        {$c$};

%     \path (A) edge node {$0.8$} (B)
%               edge node [swap]{$0.2$} (C);
%           % (B) edge [out=30,in=330,looseness=7] node {$[1,1]$} (B)
%           % (C) edge [out=30,in=330,looseness=7] node {$[1,1]$} (C);

% \end{tikzpicture}
% \caption{Exemple de modèle probabiliste}\label{exProba}
% \end{figure}

% Grâce à cet aspect numérique, il est possible de classer plus précisément les systèmes relativement à une propriété de sécurité. La notion de raffinement doit alors prendre en compte ce nouvel aspect. Plus particulièrement, le système raffiné doit vérifier la propriété avec une probabilité au moins égale, au mieux plus élevée. Encore une fois, l'intérêt du raffinement est d'améliorer le modèle relativement aux propriétés.

% \subsection{Rappels de probabilité}



% Cette méthodologie est omniprésente en génie logiciel et en génie informatique. En effet, un logiciel, un protocole, un système informatique, est avant tout la compilation d'un langage formel, le langage de programmation. Qu'il soit de bas niveau -- par exemple, langage machine ou assembleur -- ou de haut niveau -- par exemple, C++ ou Java -- un langage de programmation est utilisé pour spécifier complètement un système~: le code source d'un logiciel est en définitive la spécification finale dudit logiciel. Or celui-ci est le fruit de nombreuses étapes successives~: définition des requis, choix des formats de données, définition des conventions... Ces étapes sont l'application même du principe de raffinement~: le programme se construit progressivement, en ajoutant régulièrement des fonctionnalités. Le raffinement est donc un pan naturel du génie informatique et logiciel.

Une problématique particulière se pose alors~: est-il possible de raffiner les modèles quantitatifs, nécessaires à la modélisation de ces requis non-fonctionnels, tels que la sécurité ? 

Habituellement, la sécurité est considérée à travers trois axes principaux~: la confidentialité, l'intégrité et la disponibilité. Garantir ces trois notions revient à garantir la sécurité du système considéré. Un utilisateur malhonnête désirant attaquer l'un de ses trois aspects est communément appelé adversaire ou attaquant.
\begin{itemize}
\item La \emph{confidentialité} englobe tout ce qui est lié à l'aspect privé d'un ensemble de données. Le but est de garantir que seuls les utilisateurs autorisés peuvent intéragir avec les données que l'on veut garder confidentielles. 
\item L'\emph{intégrité} concerne la modification des données. Le but est de garantir que les données sont à tout moment complètes et lisibles. Notamment, une modification peut être réalisée uniquement par un utilisateur autorisé.
\item La \emph{disponibilité} concerne l'accès aux données ou aux services. Le but est de garantir l'accès aux données à tout utilisateur autorisé. Cette troisième propriété est généralement vue comme orthogonale aux deux précédentes, puisqu'elle doit garantir un accès, alors que les autres doivent restreindre l'accès.
\end{itemize}
Ces trois axes permettent de positionner toute problématique de sécurité dans l'industrie -- par exemple, une tentative de déni de service s'attaque à la disponibilité du système, tandis qu'un adversaire essayant d'accéder à une base de données privées s'attaque à la confidentialité du système, voir à son intégrité.

L'expansion des échanges commerciaux réalisés par l'entremise des internets, les données confidentielles hébergées sur des serveurs infonuagiques, ou encore la quantité de services nécessitant un accès fiable et continu aux réseaux internets, posent quantité de problèmes concrets de sécurité~: comment être certain de l'identité et de l'honnêteté du serveur qui reçoit nos coordonnées bancaires ? L'accès à nos données confidentielles est-il réellement contrôlé et inviolable ? Jusqu'à quel point pouvons-nous assurer la disponibilité de nos services ?

% Différentes solutions ont été créées pour pallier ces problèmes -- protocole https pour sécuriser les communications web, systèmes de pare-feu pour contrôler les accès à une zone privée, systèmes d'authentification par mot de passe, etc -- autant de systèmes de défense qui permettent de limiter l'impact des attaques. 

% Cette approche de la sécurité informatique se présente donc comme un ensemble de corrections des failles laissées lors de la conception des systèmes. L'approche des méthodes formelles, quant à elle, a pour but de modéliser les requis le plus parfaitement possible, y compris les requis de sécurité. Ainsi, dans l'idéal, les mécanismes de raffinement contraignent suffisamment l'implémentation pour obtenir un système final parfait. La logique réside dans le fait que si un système est sans faille de conception, il est par définition inviolable.

% En pratique, puisqu'aucun système réel n'est parfait, les concepteurs ont toujours recours ultérieurement aux corrections de failles. La conception idéale est finalement l'association de ces deux approches -- méthodes formelles au préalable, puis ajout de systèmes de sécurité au produit final.

La modélisation de la sécurité s'avère généralement complexe. 
Alors que les propriétés de comportement -- telles que l'absence de blocage ou l'absence de famine -- sont des propriétés portant sur des ensembles de traces d'exécution du système, la sécurité d'un système se modélise par des hyperpropriétés, c'est-à-dire des propriétés portant sur des ensembles d'ensembles de traces~: 
il est nécessaire d'ajouter un niveau d'abstraction supplémentaire par rapport aux propriétés modélisant les requis fonctionnels \citep[][]{Clarkson10}. Cet ajout de complexité induit notamment que ces propriétés ne sont généralement pas conservées lorsque le système est raffiné. Autrement dit, si un modèle vérifie une propriété de sécurité, il n'est pas nécessaire que les modèles qui le raffinent vérifient la même propriété de sécurité. Ceci pose un problème majeur qui remet en cause la méthodologie de conception évoquée jusqu'alors, puisqu'il n'est plus possible d'assurer la sécurité du produit final en vérifiant uniquement les propriétés de sécurité sur le modèle initial. 

Il s'avère que certaines propriétés de sécurité ont tout de même la capacité d'être préservées par raffinement. Prouvons dans ce mémoire que l'extension de l'opacité libérale \citep[][]{BMS15} à notre cadre de recherche fait partie de ces propriétés particulières.


\section{Exemple de motivation}
Illustrons ces questionnements avec un exemple de motivation simple. Considérons un protocole simplifié de transmission sécurisée de messages entre deux parties, dont le but est de garantir la confidentialité des messages échangés. Le système est constitué d'un canal de transmission et d'un système de chiffrement. On abstrait les détails techniques de ces deux sous-protocoles, ainsi que les systèmes de vérification éventuels qu'un tel protocole peut avoir -- attente d'un accusé de réception, par exemple.

Puisqu'aucun système n'est parfait, on considère la possibilité d'une faille dans le système de chiffrement, qui laisserait passer des messages en clair dans le canal. Puisque l'on ne connaît pas la probabilité de cette faille de sécurité, on fait apparaître un intervalle de probabilité, qui contient \emph{a priori} la valeur réelle.

Finalement, on modélise le protocole par l'objet $\Spec_1$ représenté sur la figure \ref{MotivSpec}. Les différents états du modèle (attente, transmission en clair, transmission chiffrée) traduisent les différentes étapes possibles lors de la transmission d'un message. Les flèches représentent les transitions entre les différents états, c'est-à-dire la manière dont peut s'exécuter le modèle~; elles contiennent les intervalles de probabilité. Notamment, on distingue ici deux boucles d'exécution~: lorsque le système est en \emph{attente}, il a la possibilité de transmettre le message \emph{en clair}, ce qui modélise la faille précédemment décrite, ou bien de fonctionner correctement en transmettant le message de façon \emph{chiffrée}~; puis il revient à son état initial dès la transmission réalisée, dans l'attente d'un nouveau message à transmettre.

Ce modèle, que l'on appelle une spécification, est l'abstraction de toutes les implémentations du protocole décrit précédemment~: il contient toutes les probabilités d'avoir une faille à chaque boucle de transmission, sans considérer les détails derrière la méthode de chiffrement ou de transmission.

Un tel modèle permet alors une étude plus poussée du système, notamment du point de vue de la sécurité. Par exemple, il paraît évident que le but est d'éviter de passer par l'état \emph{transmission en clair} afin d'assurer le bon fonctionnement du protocole. Ce type de modèle permet l'utilisation des méthodes quantitatives~: il est possible de mesurer la probabilité de passer par cet état, à la première boucle d'exécution, à la seconde, ou n'importe quand, suivant la propriété que l'on veut mesurer. On parlera dans la suite de probabilité de briser la sécurité.

Plus précisément, puisque l'on considère une spécification non-déterministe -- les probabilités sont uniquement restreintes -- on mesure le pire cas possible, c'est-à-dire le maximum des probabilités de briser la sécurité. Par exemple, considérons la propriété ``À la première boucle de transmission, le message est transmis en clair.'', qui est un comportement qui brise la sécurité. Dans la spécification $\Spec_1$, la probabilité maximale que cette propriété soit vraie est de 1, dûe à l'intervalle de probabilité $[0,1]$ de la transition entre les états \emph{attente} et \emph{transmission en clair}. 

Dans une logique de conception de protocole, le but est alors de réduire ce risque de faille de sécurité. Pour cela, on utilise la notion de raffinement de spécification~: le modèle initial est raffiné, c'est-à-dire qu'une nouvelle spécification est trouvée, telle que toutes ses implémentations implémentent également la spécification de départ. Autrement dit, le raffinement permet de trier parmi toutes les instances du protocole de départ afin de supprimer les pires cas. 
%
% Notons $\Sigma=\{a,b,c\}$ un alphabet, désignant trois différents états d'un protocole de transmission sécurisée de messages. Le système est dans son état initial dans l'état $a$. Dans l'état $b$, le système brise la sécurité, en réalisant la transmission du message en clair. Dans l'état $c$, le système fonctionne correctement et transmet le message chiffré au destinataire. 
%
% Dans une logique de conception par raffinement, il est dans un premier temps nécessaire de modéliser le protocole. Pour cela, on le représente par le modèle $\Spec_1$, représenté figure \ref{MotivSpec}. Cette spécification est l'abstraction de toutes les implémentations du protocole décrit précédemment. La seconde étape de conception revient alors à raffiner ce premier modèle, c'est-à-dire trouver une nouvelle spécification dont toutes les implémentations implémentent également la spécification initiale. 
Cela est réalisé ici en réduisant les intervalles de probabilités des transitions~: on obtient la spécification $\Spec_2$, représentée figure \ref{MotivPts}, qui raffine de manière évidente la spécification $\Spec_1$. 

\begin{figure}[h]
\centering
\subfloat[Modèle $\Spec_1$ \label{MotivSpec}]{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.9cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    % \node[initial,state,label=100:{attente}] (A)                     {$a$};
    % \node[state,label=right:{erreur}]         (B) [above right of=A]        {$b$};
    % \node[state,label=right:{correct}]         (C) [below right of=A]        {$c$};   %%% {$c$};



    \node[initial] (A)                     {attente};
    \node[]         (B) [above right of=A]        {transmission en clair};
    \node[]         (C) [below right of=A]        {transmission chiffrée};   %%% {$c$};
    % \node[state,label={$\{a\}$}] (D) [above of=B] {$q_1$};

    \path (A) edge [bend left] node {$[0,1]$} (B)
              edge [bend right, swap]node {$[0,1]$} (C)
          (C) edge [bend right,swap] node {$1$} (A)
          (B) edge [bend left] node {$1$} (A);
\end{tikzpicture}
}
~~~~
\subfloat[Modèle $\Spec_2$ \label{MotivPts}]{
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.9cm,semithick]
      \tikzstyle{every state}=[fill=white,text=black]

    % \node[initial,state,label=100:{attente}] (A)                     {$a$};
    % \node[state,label=right:{erreur}]         (B) [above right of=A]        {$b$};
    % \node[state,label=right:{correct}]         (C) [below right of=A]        {$c$}; %%%{$c$};
    % \node[state,label={$\{a\}$}] (D) [above of=B] {$q_1$};

    \node[initial] (A)                     {attente};
    \node[]         (B) [above right of=A]        {transmission en clair};
    \node[]         (C) [below right of=A]        {transmission chiffrée};

    \path (A) edge [bend left] node {$[0.2,0.6]$} (B)    
              edge [bend right, swap]node {$[0.4,0.8]$} (C)
          (C) edge [bend right, swap] node {$1$} (A)
          (B) edge [bend left] node {$1$} (A);
\end{tikzpicture}
}
\caption{Exemple de motivation}\label{exMotiv}
\end{figure}


Il est alors possible de vérifier si le raffinement a effectivement amélioré la mesure de risque réalisée sur la spécification $\Spec_1$. Avec le même raisonnement, la propriété ``À la première boucle de transmission, le message est transmis en clair.'' a cette fois-ci une probabilité d'être vraie de 0.6 dans le pire des cas.


\FloatBarrier











% %%
% %%  CONCEPTS DE BASE
% %%
% \section{Définitions et concepts de base}  % environ 2-3 pages

% \clearpage


%%
%% OBJECTIFS DE RECHERCHE
%%
\section{Objectifs de recherche}  % 0.5 page

Nos objectifs de recherche sont liés au problème de raffinement destructif. Le but de nos travaux consiste à~:
\begin{enumerate}
	\item définir un modèle de spécification où les requis sont non-fonctionnels et sur lequel on peut définir un raffinement~;
	\item définir et calculer la mesure de sécurité d'une spécification comme étant le pire cas de ses implémentations~;
  \item prouver que la propriété de sécurité est préservée par le raffinement de spécification, afin de contourner cette problématique et de montrer que la méthodologie de conception de systèmes sécurisés est justifiable.
	% \item montrer que la vérification de cette propriété est calculable en un temps fini -- dans le cas contraire, étudier cette propriété serait vain puisqu'elle n'aurait aucune raison d'être utilisée en pratique.
\end{enumerate}


\section{Méthodologie}

Afin de remplir ces objectifs, la méthodologie utilisée est la suivante.

Le cadre théorique consiste en l'étude du modèle de la \acf{IDTMC}. Ce type de spécification, dont deux exemples sont représentés sur la figure \ref{exMotiv}, est un modèle probabiliste non-déterministe permettant d'abstraire le système étudié. L'aspect probabiliste justifie le choix de cette méthode dans le cadre de propriétés quantitatives~; l'aspect non-déterministe permet d'étudier en un seul modèle l'ensemble des instances possibles du système. En ce sens, les \ac{IDTMC} héritent des fonctionnalités de modèles quantitatifs connus que sont les Chaînes de Markov -- méthode classique pour modéliser le comportement de nombreux systèmes -- les Systèmes de Transitions Probabilistes (PTS), ou encore les Processus de Décision Markoviens (MDP) -- modèle probabiliste non-déterministe. En pratique, une \ac{IDTMC} est un \ac{PTS} dont chaque probabilité de transition est remplacée par un intervalle de probabilités, laissant ainsi un choix indénombrable de distributions pour chaque état du modèle.

Sur ce choix de spécification sont définis des raffinements~: afin d'améliorer le modèle d'un système, on le raffine, c'est-à-dire que l'on précise son comportement. En pratique, après raffinement, l'ensemble des implémentations possibles du modèle est réduit et est inclus dans l'ensemble des implémentations du modèle initial. Réitérer ce processus conduit fatalement à une unique implémentation, un modèle totalement déterministe qui spécifie complètement le comportement du système. Un tel modèle est alors un \ac{PTS}.

On définit également une méthode d'implémentation d'une spécification, l'ordonnancement, similaire à l'ordonnancement des MDP~: l'ordonnanceur exécute la spécification, et choisit, à chaque état rencontré, une distribution de probabilités parmi les distributions autorisées par la spécification. Ce moyen d'implémenter permet de simuler l'action d'un adversaire omniscient capable de manipuler le système comme bon lui semble. Étudier ceci donne ainsi les failles éventuelles laissées par la spécification aux utilisateurs.

Parmi ces failles, on se penche davantage sur des propriétés de sécurité que l'on désire mesurer. Notamment, nous étudions plus particulièrement la notion de l'opacité~: de façon informelle, l'opacité mesure la probabilité pour un observateur extérieur passif de distinguer un secret d'un message quelconque -- si cette probabilité est suffisamment faible, conformément aux requis non-fonctionnels, cela signifie que le secret est opaque dans le modèle. Dans une spécification, la mesure de l'opacité est sa mesure dans la pire de ses implémentations, ce qui constitue une extension de la définition d'opacité au cadre des spécifications. Plus particulièrement, on définit également la mesure dans la pire de ses implémentations ordonnancées.

Toutes ces méthodes aboutissent alors à l'étude de l'effet du raffinement de spécifications sur la mesure de l'opacité.

%%
%% PLAN DU MEMOIRE
%%
\section{Plan du mémoire}  % 0.5 page
%%%%%%%%%%%% ATTENTION NOMBRE DE CHAPITRES
Le mémoire se divise en sept chapitres. À la suite de cette introduction, une revue de littérature présente l'état de l'art en ce qui concerne les méthodes formelles de spécification et de raffinement de systèmes sécurisés. 

Le chapitre \ref{sec:Prelim} rappelle des résultats préliminaires et introduit les définitions nécessaires pour la suite. Notamment, les notions de système de transition et d'automate sont expliquées, ainsi que la propriété d'opacité, propriété de sécurité choisie pour nos résultats. Les notions sont expliquées en suivant la logique suivante~: du modèle le plus simple au plus complet. Ainsi, les premiers systèmes de transition décrivent des systèmes hautement déterministes et très limités. Puis les modèles gagnent en généralité, en ajoutant notamment l'aspect probabiliste.

Le chapitre \ref{sec:Theme1} présente le cadre théorique du mémoire. Les modèles de spécifications particuliers à notre étude, le processus de raffinement utilisé, ainsi que la généralisation du concept d'opacité à ce type de modèle sont définis. 

Le chapitre \ref{sec:Resultats} prouve les résultats principaux de cette étude : la décidabilité de l'opacité libérale dans le cas des spécifications non-modales, le calcul d'une approximation de l'opacité libérale dans le cas général, et la préservation de l'opacité par raffinement faible de spécification.

Le chapitre \ref{sec:Theme2} consiste en un exemple pratique d'application des aspects théoriques mis en œuvre dans le chapitre \ref{sec:Resultats}. Un système de contrôle d'accès à une base de données est modélisé puis raffiné. Le but est de vérifier sur un exemple simple les résultats précédents, tout en montrant comment appliquer les méthodes de calcul d'opacité.

Le chapitre \ref{sec:Conclusion} résume les résultats et contributions, puis décrit les travaux futurs à déployer.


%% Parler de la théorie de la spécification~: LTS, puis MC, puis PTS, puis MDP, puis IDTMC/IMDP (raffinement, spéc)
%% Parler de l'opacité dans les LTS, puis dans les PTS, puis dans les IDTMC (ALGO + opacité restrictive + opacité Hadji)
%% Parler de la préservation par raffinement (faible, donc fort -- ok pour libérale et Hadji, mais pas nécessairement restrictive ?)
%% Donner un exemple d'application (contrôle d'accès à base de données)








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASTUCE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On veut éviter que la figure et le tableau soient placés au-delà de la section courante.
%%\FloatBarrier