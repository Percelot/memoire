reset
set terminal png
set output "restrictive.png"  # Nom du fichier de sortie
### set title textcolor rgb "red" "Mon exemple"  # Titre du graphique, de couleur rouge
set xlabel "p2"  # Nom de l'axe x
set xrange [0.1:0.9]
set ylabel "p3"  #Nom de l'axe y
set yrange [0.1:0.9]
set zlabel offset +6,+6 "f(p2,p3)"  # Nom de l'axe z et repositionnement au-dessus
f(x,y)=(x*(1-y))/(1-x*y)+(1-x)/((1-x*y)**2)-1
set pm3d  # Colorisation de la surface
set hidden3d  # Masquage du quadrillage
set isosamples 80,80  # Dimensionnement des entre-axes de la surface
splot f(x,y) with pm3d at s  # Création du graphique 3D, avec splot


