reset
set terminal png
set output "fig-bfs.png"
set key outside below;set key reverse;
set object 1 rect from 0.5,0 to 1,1 fc rgb "gold" behind
set label 1 at 0.85,0.9 "Domaine"
set label 2 at 0.5,0.5 left offset char 1,1 point pointtype 7 pointsize 2 lc rgb 'black' "BFS 1" front
set label 3 at 1,0 right offset char 1,1 point pointtype 7 pointsize 2 lc rgb 'black' "BFS 2" front
set xlabel ' x '
set ylabel ' y '
set grid xtics ytics
set samples 300
plot [t=0:1][0:1] 1-t title "Droite d'équation x+y=1"

