reset
set terminal png
set output "fig-domaine.png"
set key outside below;set key reverse;
set object 1 rect from 0.2,0.2 to 1,1 fc rgb "gold" behind
set label 1 at 0.85,0.9 "Domaine"
set label 2 at 0.2,0.8 left offset char 1,1 point pointtype 7 pointsize 2 lc rgb 'black' "BFS 1" front
set label 3 at 0.8,0.2 left offset char 1,1 point pointtype 7 pointsize 2 lc rgb 'black' "BFS 2" front
set xlabel ' P(q1,s1) '
set ylabel ' P(q2,s1) '
set grid xtics ytics
set samples 300
plot [t=0:1][0:1] 1-t title "Droite d'équation P(q1,s1)+P(q2,s1)=1"

