\section{Monotonicity of disclosure}
\label{sec:properties}

%\subsection{Monotonicity of opacity under refinement}
\label{sec:monotonicity}


This section is devoted to the proof of the following result,
establishing monotonicity with respect to weak refinement for the
disclosure over scheduled implementations:
\begin{theorem}\label{th:simulation-opacity}
  Let $\Spec_1$ and $\Spec_2$ be LCMC specifications such that
  $\Spec_1 \preceq_w \Spec_2$. Then $Disc(\Spec_1,\obs,\fee) \leq
  Disc(\Spec_2,\obs,\fee)$.
\end{theorem}

Since scheduling is a restrictive way to derive implementations from a
specification, it is not the case in general that $\sat(\Spec_1)
\subseteq \sat(\Spec_2)$: although any scheduling $\Spec_1(A_1)$ of
$\Spec_1$ with $A_1$ is an implementation of $\Spec_2$, 
this implementation may not be a
scheduling. 

Instead, the proof builds a scheduler $A_2$ for $\Spec_2$, producing
an implementation $\Spec_2(A_2)$ that is \emph{refined} by $\Spec_1(A_1)$
(Theorem~\ref{thm-refinement}, illustrated in
\figurename~\ref{fig:diagPreuve}).  Then, this refinement is shown to
ensure that the probabilities of (cones of) finite words coincide
(Propositions~\ref{prop-TraceEquiv} and~\ref{prop-TraceEquivValue}).
The disclosure set being a measurable event, coincidence of
probabilities on cones ensures coincidence of probabilities for the
disclosure.

\medskip
\noindent\textbf{Notations.}
Given two specifications $\Spec_1$ and $\Spec_2$ such that $\Spec_1$
weakly refines $\Spec_2$ through relation $\Rel$, we define the
relation $\sim$ on $\FRuns(\Spec_1) \times \FRuns(\Spec_2)$ by:
$\rho_1 \sim \rho_2$ if $|\rho_1| = |\rho_2|$ and at any intermediate
step $i$, the corresponding states satisfy $s_{1,i} \Rel \ s_{2,i}$.

% Let $A_1$ and $A_2$ be two schedulers of $\Spec_1$ and $\Spec_2$,
% respectively.  We set $\A_1 = \Spec_1(A_1)$ and $\A_2 = \Spec_2(A_2)$,
% with respective sets of states $Q_1$ and $Q_2$.
For $\rho_2 \in \FRuns(\Spec_2)$, we set $sim(\rho_2) = \{ \rho_1 \in
\FRuns(\Spec_1) \mid \rho_1 \sim \rho_2\}$. We now define a measure
$\mu_{\rho_2}$ over $sim(\rho_2)$ by $\mu_{\rho_2}(\rho_1)=
\frac{\prob_{\A_1}(\rho_1)}{\prob_{\A_1}(sim(\rho_2))}$ (where the
probability of finite run $\rho$ is abusively written instead of the
probability of its cone $C_\rho$).


\begin{figure}
\centering
\begin{tikzpicture}[auto, node distance=1.5cm and 4cm,font=\large]
%\useasboundingbox (-0.875,0.25) rectangle (4.5,-1.75);
\node (s2) at (0,0) {$\Spec_2$};
\node[left = of s2] (s1) {$\Spec_1$};
\node[below = of s1] (impl1) {$\Spec_1(A_1)$};
\node[below = of s2] (impl2) {$\Spec_2(A_2)$};

\path[<-] (s2) edge[swap] node {$\Rel$} (s1);
\path[<-] (s1) edge[swap] node {$sat_1$} (impl1);
\path[<-,dotted] (s2) edge node {$\Rel\circ sat_1$} (impl1);
\path[<-,dashed] (impl2) edge node {$\Rel'$} (impl1);
\path[<-,dashed] (s2) edge node {$sat_2$} (impl2);

\end{tikzpicture}
\caption[The result of Theorem~\ref{thm-refinement}.]{Result of Theorem~\ref{thm-refinement}. Relation $\Rel\circ sat_1$ always exists but might not be a scheduling.}
\label{fig:diagPreuve}
\end{figure}

We first show how to build a scheduler $A_2$ for $\Spec_2$ such that
$\Spec_1(A_1)$ refines $\Spec_2(A_2)$. Recall from
Section~\ref{subsec:sat} that for PTSs, weak and strong refinement
coincide and are thus simply called \emph{refinement} in the remainder
of this section.
%The proof of the theorem below is given
% in Appendix~\ref{app:refinement}.
%in~\cite{imdp2015}.
\begin{theorem}
\label{thm-refinement}
Let $\Spec_1$ and $\Spec_2$ be LCMC specifications such that $\Spec_1$
weakly refines $\Spec_2$. Then for any $A_1 \in Sched(\Spec_1)$ there
exists $A_2 \in Sched(\Spec_2)$ such that $\Spec_1(A_1)$ refines
$\Spec_2(A_2)$.
\end{theorem}

\begin{proof}
  Let $\Spec_1=(S_1, s_{init,1}, T_1, \lambda_1)$ and $\Spec_2=(S_2,
  s_{init,2}, T_2, \lambda_2)$ be LCMCs such that $\Spec_1$ refines
  $\Spec_2$ with $\Rel$. Let $sat_1$ be the satisfaction relation
  related to $A_1$ and $\A_1 = \Spec_1(A_1) =
  (Q_1,q_{init,1},\Delta_1,L_1)$. Then we show that there exists $A_2
  \in Sched(\Spec_2)$ and a refinement relation $\Rel'$ such that $
  \Rel \circ sat_1 = sat_2 \circ \Rel'$ where $sat_2$ is the
  satisfaction relation related to $A_2$.

  Let $\rho_2 \in \FRuns(\Spec_2)$ with $\lst(\rho_2)=s_2$. Then, for
  any $\rho_1 \in sim(\rho_2)$, $A_1(\rho_1) \in T_1(\lst(\rho_1))$
  and $\lst(\rho_1) \Rel\ s_2$.  Since $\Spec_1$ weakly refines
  $\Spec_2$, there exists $\delta_{\rho_1}: S_1 \to Dist(S_2)$ such
  that $\sum_{s'_1 \in S_1} A_1(\rho_1)(s'_1)\delta_{\rho_1}(s'_1) \in
  T_2(s_2)$.  

We define $A_2$ on $\FRuns(\Spec_2)$ by:
$$A_2(\rho_2) = \sum_{\rho_1 \in sim(\rho_2)} \mu_{\rho_2}(\rho_1) 
\sum_{s'_1 \in S_1} A_1(\rho_1)(s'_1) \cdot \delta_{\rho_1}(s'_1).$$
From the definition of $\mu_{\rho_2}$ and the convexity of $T_2(s_2)$,
we can conclude that $A_2(\rho_2)$ also belongs to $T_2(s_2)$, hence
$A_2$ is a scheduler of $\Spec_2$.


Writing now $\A_2 = \Spec_2(A_2) = (Q_2,q_{init,2},\Delta_2,L_2)$, the
relation $\Rel'$ can be defined as $\sim$ (relating runs that are
similar ``step by step'', as defined above). To see that the
conditions are satisfied, let $\rho_1$ and $\rho_2$ be runs in $Q_1$
and $Q_2$ respectively. Then the mapping $\delta': Q_1 \rightarrow
\dist(Q_2)$ is obtained by: $$\delta'(\rho_1)(\rho_2) =
\left\{ \begin{array}{l@{ }l} \mu_{\rho_2}(\rho_1)
    \delta_{\rho_1}(\lst(\rho_1))(\lst(\rho_2)) \ & \
    \mbox{if} \ \rho_1 \sim \rho_2 , \\
    0 & \ \mbox{otherwise.}
\end{array}\right.$$

Since $\A_1$ and $\A_2$ are PTSs, we just need to show that Equation
(\ref{simrel-probaPTSs}) holds. Writing $\rho'_2 = \rho_2
\xrightarrow{A_2(\rho_2)} s_2'$, we have:

\begin{eqnarray*}
\Delta_2(\rho_2)(\rho_2') &=& A_2(\rho_2)(s_2') \\
&=& \sum_{\rho_1 \in sim(\rho_2)} \mu_{\rho_2}(\rho_1) \sum_{s'_1 \in S_1} A_1(\rho_1)(s'_1) \cdot \delta_{\rho_1}(s'_1)(s'_2) \\
&=&  \sum_{\rho_1 \in sim(\rho_2)}  \sum_{s'_1 \in S_1} A_1(\rho_1)(s'_1) \cdot \delta'(\rho_1 \xrightarrow{A(\rho_1)} s'_1)(\rho'_2) \\
&=&  \sum_{\rho_1' \in Q_1} A_1(\rho_1)(s'_1) \cdot \delta'(\rho_1')(\rho'_2) 
\end{eqnarray*}
by defining $\rho_1' = \rho_1 \xrightarrow{A_1(\rho_1)} s'_1$ for each $\rho_1$ and remarking that $\delta'=0$ if its arguments are not similar runs. Hence:
\[\hspace{2.45cm}\Delta_2(\rho_2)(\rho_2') =  \sum_{\rho_1' \in Q_1} \Delta_1(\rho_1)(\rho'_1) \cdot \delta'(\rho_1')(\rho'_2). 
\hspace{2.45cm}
\qed
\]
 
%$$\sum_{\rho'_1 \in Q_1} \Delta(\rho_1)(\rho'_1)\times 
%\delta'(\rho'_1)(\rho'_2) 
%= \Delta(\rho_2)(\rho'_2) $$ which comes from the fact that the
%lefthand side is equal to:
% $$\sum_{\rho_1 \in sim(\rho_2)} \mu_{\rho_2}(\rho_1) 
%\sum_{s'_1 \in S_1} A_1(\rho_1)(s'_1) \times \delta(s'_1)(s'_2),$$
%while $\Delta(\rho_2)(\rho'_2) = A_2(\rho_2)(s'_2)$.
\end{proof}
%
%\MS{On ne sait pas si c'est vrai\dots}
%\begin{corollary}
%\label{cor-PGrandeSim}
%Let $\Spec_1$ and $\Spec_2$ be IDTMC specifications such that
%$\Spec_2$ simulates $\Spec_1$. Given $A_1 \in Sched(\Spec_1)$, let
%$A_2 \in Sched(\Spec_2)$ and $\Rel'$ as defined in the proof above
%then $\Rel'$ is the largest simulation s.t. $\Spec_2(A_2)$ simulates
%$\Spec_1(A_1)$.
%\end{corollary}
%
%\begin{proof}
%\end{proof}

%\smallskip

Now we show that refinement between two PTSs is sufficient to compare
their disclosure.  Namely, we show that the probabilities of cones of
words are equal in both systems.  Note that although this property is
well known to hold for paths, it needs to be lifted to words in order
to compare disclosure.

We start by considering the sets of traces globally; although it is
folklore that refinement implies trace inclusion, we provide a proof
for completeness sake.

\begin{proposition}
\label{prop-TraceEquiv}
Let $\A_1$ and $\A_2$ be  PTSs such that $\A_1$ refines $\A_2$.\\
Then $Tr(\A_1) = Tr(\A_2)$.
\end{proposition}

\begin{proof}
  We prove the proposition by induction on a strengthened statement.
  Namely, we claim that for every finite run in $\A_1$ there exists a
  similar run in $\A_2$.  Since an infinite run is the limit of the
  sequence of its finite prefixes, this claim is sufficient to prove
  the proposition.  Assume by induction that the proposition holds for
  every word of length $n$.  Let $w \in \FTr(\A_1)$ of length $n+1$.
  We write $w = w_0 a$ for some $a \in \Sigma$.  Consider a run of
  $\A_1$ that produces $w$.  It is of the form $\rho_0 s_1'$ where
  $\lambda(s_1') = a$; let $s_1 = \lst(\rho_0)$.  Let $\rho_0'$ be a
  run in $\A_2$, similar to $\rho_0$, and $s_2 = \lst(\rho_0')$.  By
  definition of refinement, there exists a function $\delta$ such that
  for any state $s_2'$ of $\A_2$,
\begin{equation*}
  \Delta_2 (s_2)(s'_2) = \sum_{\sigma_1 \in S_1} \Delta_1(s_1)(\sigma_1)\cdot \delta(\sigma_1)(s'_2).
\end{equation*}
Moreover, whenever $\delta(\sigma_1)(s'_2)>0$, $\lambda(s_1') =
\lambda(s_2')$.  Since $\delta(s_1')$ is a distribution over $S_2$,
$\delta(s'_1)(s'_2)>0$ for at least one state $s_2'$.  Hence $\rho_0'
s_2'$ is similar to $\rho$, which shows in particular that $w \in
\FTr(\A_2)$. 
\qed
\end{proof}

We additionally show that probabilities coincide:
\begin{proposition}
\label{prop-TraceEquivValue}
Let $\A_1$ and $\A_2$ be  PTSs such that $\A_1$ refines $\A_2$.
Then for all $w\in \Sigma^*$, $\prob_{\A_1}(C_w) = \prob_{\A_2}(C_w)$. 
\end{proposition}
Since a given word may be produced by several paths, their
probabilities should be considered altogether.  Hence the proof of the
above proposition is not immediate; it is quite technical and can be
found  in Appendix~\ref{app:proof:prop-TraceEquivValue}.
%found in~\cite{imdp2015}.

\medskip

Existing properties about refinement for PTSs can be retrieved as
consequences of the above result.  They were for example obtained as a
particular case of sub-stochastic refinement in~\cite{baier05}.
Although not necessary to prove the main theorem, these results
illustrate how constraining refinement between PTSs is.

Recall that a probabilistic bisimulation~\cite{larsen-jonsson} is a
bisimulation that preserves transition probabilities, \emph{i.e.},  a
bisimulation relation $\Rel$ on states such that for any equivalence
class $R$ of $\Rel$, and any two related states $s\Rel s'$,
$\Delta(s)(R)=\Delta(s')(R)$.

%\noindent \textbf{Corollary 1.}~(\cite{baier05})\label{cor:prob-bisimulation}
\begin{corollary}[\cite{baier05}]\label{cor:prob-bisimulation}
Let $\A_1$ and $\A_2$ be PTSs such that $\A_1$ refines $\A_2$.
Then there exists a probabilistic bisimulation over the union of both PTSs.
\end{corollary}

%\begin{proof}[Sketch]
%  We define the relation $\Rel_{bisim}$ as $(\Rel_{sim} \cup
%  \Rel_{sim}^{-1})^*$ where $\Rel_{sim}$ is the simulation relation
%  between $\A_2$ and $\A_1$, and show that it satisfies the
%  conditions of a probabilistic bisimulation.
%\end{proof}

%\noindent \textbf{Corollary 2.}~(\cite{baier05})
\begin{corollary}[\cite{baier05}]\label{cor:prob-refinement2sens}
Let $\A_1$ and $\A_2$ be PTSs such that $\A_1$ refines $\A_2$.
Then $\A_2$ also refines $\A_1$.
\end{corollary}

%\begin{proof}
%  Let $\Rel$ be the simulation relation between $\A_1$ and $\A_2$.
%  For a state $s$, $[s]$ denotes the equivalence class of $s$ with
%  respect to the probabilistic bisimulation $\Rel_{bisim}=(\Rel \cup
%  \Rel^{-1})^*$. The correspondence of labels is straightforward by
%  $\Rel$.  We define the function $\delta$ as follows:
%\[
%\delta_{s_1,s_2}(s_1')(s_2') = \left\{\begin{array}{ll}
%    \frac{\Delta_2(s_2)(s_2')}{\sum_{\sigma \in [s_1]\cap S_1}\Delta_1(s_1)(\sigma)} & \mbox{if } s_1' \Rel s_2' \\
%    0 & \mbox{otherwise}
%\end{array}\right.
%\]
%This function satisfies Condition~(\ref{simrel-proba}) of
% Definition~\ref{def-simulation} thanks to the properties of
% probabilistic bisimulation $\Rel_{bisim}$: \[\sum_{\sigma \in
%   [s_1]\cap S_1}\Delta_1(s_1)(\sigma) = \sum_{\sigma \in [s_2]\cap
%   S_2}\Delta_2(s_2)(\sigma).\] Function $\delta$ also satisfies the
% third item of Definition~\ref{def-simulation} by construction.  \qed
%\end{proof}


%Let us consider again specifications reduced to PTSs obtained by schedulers. 
%Thanks to Proposition~\ref{prop-refinement}, the following result holds. 


We are now ready to prove Theorem~\ref{th:simulation-opacity}:
\begin{proof}
Let $\A_1 \in \sat(\Spec_1)$.  By
Theorem~\ref{thm-refinement} there exists $\A_2 \in \sat(\Spec_2)$
that is refined by $\A_1$.  By Proposition~\ref{prop-TraceEquivValue},
$\prob_{\A_1}(C_w) = \prob_{\A_2}(C_w)$ for every word $w\in
\FTr(\A_1)$.  Hence, for any $\omega$-regular (hence measurable)
language $\lang$, one has $\prob_{\A_1}(\lang) =
\prob_{\A_2}(\lang)$. It is in particular the case for
$\V(\A_1,\obs,\fee)=\V(\A_2,\obs,\fee)$.  Therefore,
$Disc(\A_1,\obs,\fee) =
Disc(\A_2,\obs,\fee)$. Consequently, the theorem holds.
\qed
\end{proof}
%\fi

%\subsection{Compositional properties of disclosure}
The result above can now be combined with compositional results on
refinement obtained in~\cite{CaillaudDLLPW11}. In particular, since the
conjunction of two CMC specifications is the greatest lower bound with
respect to weak refinement, and LCMCs are closed under conjunction,
we have:
\begin{proposition}
\label{prop-conjunction}
Let $\Spec_1$ and $\Spec_2$ be LCMC specifications.  Then
$Disc(\Spec_1 \wedge \Spec_2) \leq \min(Disc(\Spec_1),
Disc(\Spec_2))$.
\end{proposition}



