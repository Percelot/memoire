\section{Computing the probabilistic disclosure}\label{sec:compute}

\subsection{Modal edges}

When the probability of an edge can be equal to $0$, 
%this edge may be present or not in an implementation. Otherwise said, 
%in the case of schedulers, this action
the corresponding action can be completely blocked by a scheduler.  
%In keeping with the terminology of
From a modeling point of view, modal edges add a lot of flexibility
for refinement.  This however means that the range of potential
implementations is larger and so it will be harder to obtain meaningful
properties. Therefore such edges are desirable in an early modeling
phase but less so in the latest refinements.
As in~\cite{larsen-jonsson}, we call these edges \emph{modal}
edges, and CMCs that contain such edges are called \emph{modal CMCs}.

\begin{definition}[Modal edge]
  An edge $(s,s')$ in CMC $\Spec$ is \emph{modal} if there exists
  a scheduler $A$ such that $\Delta(\rho)(\rho\xrightarrow{A(\rho)} s')=0$ for any run $\rho$ of $\Spec(A)$ with $\lst(\rho)=s$.
\end{definition}


\begin{figure}%[t!]
\tikzset{every state/.style={minimum size=12pt,inner sep=1.5pt}}
%\vspace*{-0.66cm}
\tikzset{every loop/.style={min distance=5mm,in=45,out=105,looseness=5}}
\centering
~~~~\subfigure[A modal IMC $\Spec_{\textrm{m}}$.]{\label{fig:modalImdp}%
\begin{tikzpicture}[auto,xscale=1]
%\useasboundingbox[draw=red] (-0.75,-0.25) rectangle (4,1.33);
\node[state,initial] (init) at (0,0) {$a$};
\node[state] (sec) at (1.75,0) {$c$};
\node[state] (fin) at (3.5,0) {$b$};

\path[->] (init) edge node {$[0;1]$} (sec)
                 edge[bend left=60] node {$[0;1]$} (fin);
\path[->] (sec) edge node {$[1;1]$} (fin);
\path[->] (fin) edge[loop above] node {$[1;1]$} (fin);
\end{tikzpicture}
}
\hfill
\subfigure[A non-modal IMC $\Spec_{\textrm{nm}}$.]{\label{fig:noModalImdp}%
\begin{tikzpicture}[auto,xscale=1]
%\useasboundingbox[draw=red] (-0.75,-0.25) rectangle (4,1.33);
\node[state,initial] (init) at (0,0) {$a$};
\node[state] (sec) at (1.75,0) {$c$};
\node[state] (fin) at (3.5,0) {$b$};

\path[->] (init) edge node {$]0;1]$} (sec)
                 edge[bend left=60] node {$]0;1]$} (fin);
\path[->] (sec) edge node {$[1;1]$} (fin);
\path[->] (fin) edge[loop above] node {$[1;1]$} (fin);
\end{tikzpicture}
}~~~~

~~~~\subfigure[A disclosing implementation of $\Spec_{\textrm{m}}$ (but not of $\Spec_{\textrm{nm}}$).]{\label{fig:implModal}%
\begin{tikzpicture}[auto,xscale=1]

%\useasboundingbox[draw=red] (-0.875,-0.225) rectangle (4,1.1);

\node[state,initial] (init) at (0,0) {$a$};
\node[state] (sec) at (1.75,0) {$c$};
\node[state] (fin) at (3.5,0) {$b$};

\path[->] (init) edge node {$1$} (sec);
\path[->] (sec) edge node {$1$} (fin);
\path[->] (fin) edge[loop above] node {$1$} (fin);
\end{tikzpicture}%
}
\hfill
\subfigure[A non-disclosing implementation of $\Spec_{\textrm{nm}}$ (and $\Spec_{\textrm{m}}$), $\varepsilon>0$.]{\label{fig:implNoModal}%
\begin{tikzpicture}[auto,xscale=1]
%\useasboundingbox[draw=red] (-0.875,-0.225) rectangle (4,1.25);
\node[state,initial] (init) at (0,0) {$a$};
\node[state] (sec) at (1.75,0) {$c$};
\node[state] (fin) at (3.5,0) {$b$};

\path[->] (init) edge node {$1-\varepsilon$} (sec)
                 edge[bend left=60] node {$\varepsilon$} (fin);
\path[->] (sec) edge node {$1$} (fin);
\path[->] (fin) edge[loop above] node {$1$} (fin);
\end{tikzpicture}
}~~~~
\caption{The influence of modal transitions on disclosure.}
\label{fig:modality}
\end{figure}

In the context of opacity, removing an edge drastically changes the
disclosure, since it can remove ambiguities.  
For example, consider the modal IMC $\Spec_{\textrm{m}}$ of
\figurename~\ref{fig:modalImdp}, where $a$ and $b$ are observed and
the secret is the presence of $c$.  An implementation of
$\Spec_{\textrm{m}}$ that blocks the direct edge from $a$ to $b$
(\figurename~\ref{fig:implModal}) has a disclosure of $1$, since the
secret is guaranteed to be part of the only possible run.  On the
other hand, in the non-modal version of the IMC
(\figurename~\ref{fig:noModalImdp}), such implementations are banned
and only implementations that retain a small probability to avoid $c$
are allowed.  In these implementations, the disclosure is $0$, since
every run is observed as $ab^\omega$ and it is possible that $c$ did
not occur.
 
The detection of modal edges is the first step toward computation of
the disclosure.

\begin{proposition}\label{thm:MEdetection}
  The set of modal edges can be computed in time polynomial in the
  number of edges.
\end{proposition}

\begin{proof}
%The decision procedure for each edge is as follows:
%\begin{itemize}
%\item if an edge is not weighted by an interval containing $0$, it is
%  not modal;
%\item otherwise, compute the sum of interval upper bounds of all
%  other edges stemming from the same state;
%\begin{itemize}
%\item if this sum is $>1$, the edge is modal;
%\item if this sum is $<1$, the edge is not modal;
%\item otherwise (the sum is $=1$), the edge is modal if, and only if,
%  all intervals of other outgoing edges are closed on the right.
%\end{itemize}
%\end{itemize}
  The procedure for each edge $(s,s')$ is as follows.  Assume $T(s)$
  is defined by the conjunction $\bigwedge_{i=1}^k C_i$ where each
  $C_i$ is a linear constraint.  The edge $(s,s')$ is modal if and
  only if $x_{s'}=0 \wedge \bigwedge_{i=1}^k C_i$ admits a solution.
  This can be checked in polynomial time~\cite{RoTeVi97}.
\qed
\end{proof}

%Note that the procedure does not rely entirely on the syntactic
%criterion of an interval closed on $0$: it is sufficient but may lead
%to false positives.  For example, consider a state with two outgoing
%edges $e_1,[\frac14;\frac23]$ and $e_2,[0;1]$.  The $e_2$ edge is not
%actually modal since any probability distribution satisfying the
%specification can give at most $\frac23$ to $e_1$, hence must at least
%give weight $\frac13$ to $e_2$.  This is avoided by the
%pre-computation of the least possible probability that can be put on
%an edge.

\subsection{Computation in non-modal LCMCs}

%In the case of non-modal IMCs, disclosure can be computed:  
%%The proof
%%relies on a translation from IMC to MDP and synchronization with DPA.
%
%\begin{theorem}\label{thm:computNoModal}
%  Computing the value of disclosure for an IMC $\Spec$
%  without modal edges can be done in 2EXPTIME.
%\end{theorem}
%
%\begin{proof}
%  Note that intervals may be closed or open on any non-zero bound,
%  which is not the case of IMCs in~\cite{ChatterjeeHenzinger2008}
%  where all intervals are closed.  Hence our procedure adapts ideas
%  from this work to deal with the general case.
%%  although our procedure uses some of the ideas from this work,
%%of Chatterjee \emph{et al}~\cite{ChatterjeeHenzinger2008}, 
%% it must be adapted to deal with open intervals.
%
%  First remark that there exists a regular language $K$ such that for
%  any scheduler $A$, $\Tr(\Spec(A))=K$.  This is only true because
%  $\Spec$ is assumed non-modal.  Let $\A_K$ be a PTS such that
%  $\Tr(\A_K)=K$; it can be chosen of size $|\Spec|$.  By the
%  definition of disclosure, if the secret $\varphi$ is
%  $\omega$-regular and the observation function $\obs$ is a projection,
%  %(see Section~\ref{sec:bg-opacity}), 
%  then finding the supremum of the disclosure means finding the
%  maximal probability to reach an $\omega$-regular set of runs, namely
%  $\V(\A_K,\obs,\fee)$.
%  Then we claim that open intervals can be handled as closed ones 
%%can be safely ignored 
%  when trying to optimize the probability of $\V(\A_K,\obs,\fee)$.
%% \emph{i.e.} they are treated as closed ones.  
%  Indeed, if the optimal scheduler uses a value $x$ which is the bound
%  of an open interval, then one can build a family of schedulers using
%  value $x\pm\frac1{2^n}$ for the $n$th scheduler.  The limit
%  probability of reaching $\V(\A_K,\obs,\fee)$ is therefore the one
%  computed when using exact value $x$.  Remark that using closed
%  intervals may introduce intervals containing $0$, although it is of
%  no concern since the observation classes are already defined and may
%  not change, only their probability may change.  Said otherwise, this
%  does not mean that we are computing disclosure of the closed
%  version, since it is only a probability.  On the example of
%  \figurename~\ref{fig:noModalImdp}, it means trying to compute the
%  maximal probability of the empty set, which is indeed zero.
%
%The procedure is hence as follows.
%  Starting from a DPA $\A_\fee$ for $\fee$, a DPA $\A_\V$ for
%  $\V(\A_K,\obs,\fee)$ can be built, with size exponential in the size
%  of $\Spec$ and $\A_\fee$ (and with a number $k$ of colors polynomial in the
%  size of $\A$ and $\A_\fee$).  This construction relies on
%  intersections and complementations of DPA, with a determinization
%  step that brings the exponential blowup~\cite{Piterman07}.
%
%  The construction of~\cite{ChatterjeeHenzinger2008} yields a
%  memoryless scheduler, although it is memoryless on the product, and
%  hence is finite-memory on the original IMC.  The procedure
%  of~\cite{ChatterjeeHenzinger2008} is in EXPTIME with respect to the
%  size of its input, hence computation of disclosure is doubly
%  exponential: $2^{2^{O\left(|\A|\times|\A_{\varphi}|\right)}}$.
%\end{proof}

In the case of non-modal LCMCs, the disclosure can be computed:
\begin{theorem}\label{thm:computNoModal}
  Computing the value of disclosure for an LCMC $\Spec$
  without modal edges can be done in 2EXPTIME.
\end{theorem}

\begin{proof}
  The proof relies on constructing an (exponentially larger) MDP on
  which an optimization problem is solved.  In the spirit
  of~\cite{SenVA06}, the construction of the MDP relies on basic
  feasible solutions (BFSs), otherwise called corner points.

Starting from a DPA $\A_\fee$ for $\fee$, a DPA $\A_\V$ for
$\V(\A_K,\obs,\fee)$ can be built, with size exponential in the size
of $\Spec$ and $\A_\fee$ (and with a number $k$ of colors polynomial
in the size of $\A$ and $\A_\fee$).  This construction relies on
intersections and complementations of DPA, with a determinization step
that brings the exponential blowup~\cite{Piterman07}.  Synchronizing
the DPA $\A_\V$ with the original LCMC yields an LCMC $\Spec_\V$.
Finding the optimal scheduler for $\Spec_\V$ to accept yields the
optimal value of $Disc(\A_K,\obs,\fee)$.  This optimization is done by
translating $\Spec_\V$ into an MDP $\mathcal{M}_\V$ as follows.

For each state $s$ of $\Spec_\V$, we compute the set of the BFS of the
polytope defined by $T(s)$.  Then we build the corresponding state
in the MDP $\mathcal{M}_\V$ by adding a transition (i.e. a probability
distribution) per BFS.  As a property of BFSs, any distribution in
$T(s)$ in $\Spec_\V$ can be expressed as a barycentric combination of
BFSs.  Hence a scheduler on $\mathcal{M}_\V$ corresponds exactly to a
scheduler on $\Spec_\V$.  As a result maximizing the probability of
$\V(\A_K,\obs,\fee)$ in $\mathcal{M}_\V$ is exactly the same as
computing said optimum in $\Spec_\V$.  This yields a
memoryless scheduler, which in turn can be translated into a finite
memory scheduler in $\Spec$.

Note that this construction annihilates the difference between strict
and large inequalities.  This is of no consequence on the computation
of the value of the disclosure.  Indeed, if the optimal value is
reached on a facet of the polytope defined through a strict
inequality, one can build a sequence of ever closer schedulers
converging in the limit to the optimal value.  In the case where the
strict constraint is of the form $x_s >0$, i.e. that the edge should
disappear in the limit, this actually does not introduce a modal edge
in the sense that the set $\V(\A_K,\obs,\fee)$ is not changed.

The number of BFSs of a system of rank $r$ in dimension $n$ is
bounded by $\left(n \atop r\right)$ (the number of subsets of cardinality $r$ in a set of $n$ elements), hence for each state there is an exponential number
of BFSs to consider.  As a result the overall complexity of the
procedure is in 2EXPTIME.
\qed
\end{proof}

An example of the transformation described in the above proof is illustrated for state $q_0$ of the LCMC in \figurename~\ref{fig:CMCequation}.
The BFSs are depicted in \figurename~\ref{fig:cornerpoints}: all possible distributions are points in the red triangle with corners $\mu_1$, $\mu_2$, and $\mu_3$ being the three BFSs.
Note that these distributions only consider values for $x_1$, $x_2$, $x_3$, since all other values are null.
The transformation of the LCMC (for state $q_0$) into an MDP is illustrated in \figurename~\ref{fig:cornerpointMdp}.

\begin{figure}
\centering
\begin{tikzpicture}[scale=5,tdplot_main_coords]
\path[draw=red!30,fill=red!10] (1,0,0) -- (0.5,0.5,0) -- (0.5,0.3333333333,0) -- cycle;
\path[draw=red,fill=red,opacity=0.5] (1,0,0) -- (0.5,0.5,0) -- (0.5,0.33333333333,0.16666666667) -- cycle;
\path[draw=black,dotted] (0.5,0.33333333333,0.16666666667) -- (0.5,0.33333333333,0);
\def\markdist{0.025}
\node[font=\tiny] (cp1) at  (1,0,0) {\textbullet};
\node[font=\tiny] (cp2) at  (0.5,0.5,0) {\textbullet};
\node[font=\tiny] (cp3) at  (0.5,0.33333333333,0.16666666667)  {\textbullet};
\node[anchor=south west,font=\scriptsize] at (cp1) {$\mu_1$};
\node[anchor=west,font=\scriptsize] at (cp2) {$\mu_2$};
\node[anchor=south,font=\scriptsize] at (cp3) {$\mu_3$};
\path[draw,->] (0,0,0) -- (1.1,0,0) node[anchor=north] {$x_1$};
\path[draw,->] (0,0,0) -- (0,0.8,0) node[anchor=south east] {$x_2$};
\path[draw,->] (0,0,0) -- (0,0,0.5) node[anchor=east] {$x_3$};
\path[draw] (0.5,\markdist,0) -- (0.5,-\markdist,0) node[anchor=north east] {$\frac12$};
\path[draw] (1,\markdist,0) -- (1,-\markdist,0) node[anchor=north east] {$1$};
\path[draw] (\markdist,0.5,0) -- (-\markdist,0.5,0) node[anchor=south east] {$\frac12$};
\path[draw] (\markdist,0,0.166666667) -- (-\markdist,0,0.16666667) node[anchor=east] {$\frac16$};

\node[anchor=west] at (1.2,0.5,0.33) {\begin{tabular}{l}
$\mu_1=(1,0,0)$ \\ ~\\
$\mu_2=(\frac12,\frac12,0)$ \\ ~\\
$\mu_3=(\frac12,\frac13,\frac16)$
\end{tabular}};
\end{tikzpicture}
\caption{Basic Feasible Solutions for state $q_0$.}
\label{fig:cornerpoints}
\end{figure}

\begin{figure}
\centering
\begin{tikzpicture}[auto]
\tikzstyle{every state}+=[shape=rounded rectangle];

\begin{scope}
\node[state,initial,label=below left:$q_0$] (idle) at (0,0) {Idle};
\node[state,label=right:$q_2$] (error) at (3,0) {Error};
\node[state,label=right:$q_1$] (success) at (3,2) {Success};
\node[state,label=right:$q_3$] (dead) at (3,-2) {Failure};

\path[->] (idle) edge[bend left=30] node {$x_1$} (success);
\path[->] (idle) edge node {$x_2$} (error);
\path[->] (idle) edge[bend right=30] node[swap] {$x_3$} (dead);
\end{scope}

\begin{scope}[xshift=6.5cm]
\node[state,initial,label=below left:$q_0$] (idle2) at (0,0) {Idle};
\node[state,label=right:$q_2$] (error2) at (3,0) {Error};
\node[state,label=right:$q_1$] (success2) at (3,2) {Success};
\node[state,label=right:$q_3$] (dead2) at (3,-2) {Failure};

\path[->] (idle2) edge[out=90,in=120] node {$\mu_1,1$} (success2);
\path[->] (idle2) edge[out=60,in=180,swap,pos=0.66] node {$\mu_2,\frac12$} (success2);
\path[->] (idle2) edge[out=30,in=-120,swap,pos=0.8] node {$\mu_3,\frac12$} (success2);

\path[->] (idle2) edge[out=-30,in=-150] node {$\mu_2,\frac12$} (error2);
\path[->] (idle2) edge[out=-60,in=-90,swap] node {$\mu_3,\frac13$} (error2);

\path[->] (idle2) edge[out=-90,in=-180,swap] node {$\mu_3,\frac16$} (dead2);
\end{scope}
\node[font=\Huge] at (barycentric cs:error=1,idle2=1) {$\rightsquigarrow$};
\end{tikzpicture}
\caption{Transforming an LCMC into an MDP using BFSs.}
\label{fig:cornerpointMdp}
\end{figure}


\subsection{Towards the general case}

%\paragraph{Remarks on modal edges.}
 When a scheduler is faced with the choice to include or exclude a
 modal edge, it can produce several versions of PTSs, say $\A_1$ and
 $\A_2$, with $\Tr(\A_1)\neq\Tr(\A_2)$, hence
 $\V(\A_1,\obs,\fee)\neq\V(\A_2,\obs,\fee)$.  In addition, these
 choices may be history dependent, as in the example of
 \figurename~\ref{fig:histmodaledge}, with $\varphi=a\Sigma^\omega$ and
 only letters $c$ and $d$ being observed.  Intuitively, a way for the
 scheduler to always disclose the presence of an initial $a$ is to
 always follow an $a$ by the same letter, say a $c$.  However, this
 choice must be made after the first letter has been seen.  Moreover,
 leaving the possibility of a run $ad\cdots$ to occur means that run
 $ac\cdots$ does not disclose $\varphi$.  As a result, the scheduler
 should also take into account $\varphi$ and the observation function
 before committing to a choice with respect to modal edges.

 \begin{figure}
 \centering
 \begin{tikzpicture}[auto]
 \useasboundingbox (-0.75,-1.875) rectangle (6.25,1.875);

 \node[initial,state] (orig) at (0,0) {};
 \node[state] (a) at (2,0.5) {$a$};
 \node[state] (b) at (2,-0.5) {$b$};
 \node[state] (post) at (4,0) {};
 \node[state] (c) at (6,0.5) {$c$};
 \node[state] (d) at (6,-0.5) {$d$};

 \path[->] (orig) edge[bend left=10,pos=0.875] node {$\frac12$} (a);
 \path[->] (orig) edge[bend right=10,pos=0.875,swap] node {$\frac12$} (b);
 \path[->] (a) edge[bend left=10,pos=0.33] node {$1$} (post);
 \path[->] (b) edge[bend right=10,pos=0.33,swap] node {$1$} (post);
 \path[->] (post) edge[bend left=10,pos=0.66] node {$[0;1]$} (c);
 \path[->] (post) edge[bend right=10,swap,pos=0.66] node {$[0;1]$} (d);
 \path[->] (c) edge[bend right=60,looseness=0.75] node[swap] {$1$} (orig);
 \path[->] (d) edge[bend left=60,looseness=0.75] node {$1$} (orig);
 \end{tikzpicture}
 \caption{IMC where the choice on modal edge requires history.}
 \label{fig:histmodaledge}
 \end{figure}

 \medskip So far, the general case of modal LCMCs remains open.
 However, we now propose an approximation scheme using finite memory
 schedulers.
 
 In the case of modal LCMCs, disclosure can be approximated by
 computing only what can be disclosed by an adversary with bounded
 memory. Increasing the allotted memory provides a better approximation of the disclosure, although there is no guarantee that the disclosure can be achieved with finite memory.
A finite memory adversary can be defined as follows:
\begin{definition}[$n$-memory scheduler]
  Let $[n]$ denote the set $\{1, 2, \ldots, n\}$. An $n$-memory
  scheduler $A$ for an LCMC specification $\Spec= (S, s_{init}, T,
  \lambda)$, is a tuple $A = ([n], i_{init}, \theta, \gamma)$ where
  $[n]$ is the set of modes, $i_{init}$ is the starting mode, $\theta :
  [n] \times S \rightarrow [n]$ is a mode transition function and
  $\gamma : [n] \times S \rightarrow \dist(S)$, is the choice function
  with $\gamma(i,s) \in T(s)$ for all $i \in [n]$ and all $s \in S$.
\end{definition}

Scheduling $\Spec$ with $A$ produces a PTS $\Spec(A)$ where the set of
states is $[n] \times S$, the initial state is $(i_{init}, s_{init})$,
for $(i, s) \in [n] \times S$, $L(i,s) \in \lambda(s)$ and
$\theta(i,s)(i',s') = \gamma(i,s)(s')$ for $i'= \theta(i,s)$.  We
denote by $Sched_{n}(\Spec)$ the set of $n$-memory schedulers for
$\Spec$, with $\sat_n(\Spec)= \{ \Spec(A) \mid A \in Sched_n(\Spec)\}$
and $$Disc_n(\Spec, {\cal O}, \varphi) = sup_{A \in Sched_n(\Spec)}
Disc(\Spec(A), \obs, \fee).$$ Memoryless schedulers are those in
$Sched_{1}(\Spec)$ and the set of finite memory schedulers is
$\bigcup_{n}Sched_{n}(\Spec)$~\cite{baier-katoen}.

The computation of the disclosure of an LCMC $\Spec$ under bounded
memory adversaries relies on:
\begin{itemize}
\item the computations of the set of modal edges of $\Spec$
  (Proposition~\ref{thm:MEdetection});
\item the value of disclosure for LCMCs without modal edges
  (Theorem~\ref{thm:computNoModal});
\item the \emph{unwinding} of $\Spec$ with respect to a memory bound
  and the removal of modal edges (described below).
\end{itemize}

\paragraph{The unwinding construction.} Given $\Spec= (S, s_{init}, T,
\lambda)$ and a finite transition system ${\cal A}_{n, \theta} = ([n],
i_{init}, \theta)$, we construct the LCMC $ {\cal A}_{n, \theta}
\times \Spec = ([n] \times S, (i_{init}, s_{init}), \hat{T},
\hat\lambda)$ s.t.
\begin{itemize}
\item $\hat\mu \in \hat{T}(i,s)$ iff $\exists \mu \in T(s)$ such that
  for $i'\in [n]$,
\[\hat\mu(i',s')=\left\{\begin{array}{l}
\mu(s') \mbox{ if }i'=\theta(i,s) \\
0 \mbox{ otherwise}
\end{array}\right.\]
\item $\hat\lambda(i,s) = \lambda(s)$
\end{itemize}
The unwinding of $\Spec$ by an $n$-state automaton $\A$ produces an
LCMC $\Spec_\A$ formed with $n$ copies of $\Spec$ (one for each state
of $\A$), communicating with each other according to the mode
transition function of $\A$.  Memoryless schedulers on $\Spec_\A$ then
correspond to $n$-memory schedulers on $\Spec$.  We note $Sched_{{\cal
    A}_{n, \theta}}({\cal S})$ the set of schedulers for $\Spec$ of
the form $A = ({\cal A}_{n, \theta}, \gamma)$ for $\gamma : [n] \times
S \rightarrow \dist(S)$ where for $s\in S$ and for all $i\in [n]$,
$\gamma(i,s)\in T(s)$.  Hence, any implementation of
$\underline{sat}_n(\Spec)$ can be obtained by a memoryless scheduler
on some $\Spec_\A$. Remark that there is only a finite number of such
automata. This construction entails:
\begin{lemma}~\label{lem:rewind}  For any  LCMC $\Spec$, 
 $$\sat_1({\cal A}_{n, \theta} \times \Spec) = \{ \Spec(A) \mid A = ({\cal A}_{n, \theta},  \gamma) \mbox{ for some } \gamma\}.$$
\end{lemma}


\paragraph{The modal edge removing construction.} Given $\Spec= (S,
s_{init}, T, \lambda)$, an LCMC, let $\mathcal{M}(\Spec)$ be the set
of modal edges of $\Spec$.  For a subset $\xi \subseteq
\mathcal{M}(\Spec)$, we define $\Spec \setminus \xi$ as the modal edge
free LCMC obtained from $\Spec$ by removing edges in $\xi$ and ``\emph{unmodalizing}'' modal edges of $\Spec$ not in $\xi$: this means that
if $(s,s') \in \xi$, we add the constraint $x_{s'}=0$ to $T(s)$ and if
$(s,s') \notin \xi$, we add the constraint $x_{s'}>0$ to $T(s)$. Note
that removing an arbitrary subset $\xi$ of $\mathcal{M}(\Spec)$ may
result in an inconsistent specification. These will be ignored by our
algorithm (recall that consistency checks are polynomial for
LCMCs~\cite{CaillaudDLLPW11}).

  \paragraph{The algorithm.}
  Algorithm~\ref{algomod} enumerates all possible finite transition
  systems with $n$ states and unwinds $\Spec$ over them.  For each
  such unwinding, the algorithm explores the set of memoryless
  schedulers. This exploration is done by first selecting the set of
  modal edges to remove and for each set, compute the maximal
  disclosure using the procedure of Theorem~\ref{thm:computNoModal}.
  Since this procedure may use arbitrary schedulers, the output of the
  algorithm is an overapproximation of $Disc_n(\Spec, {\cal O},
  \varphi)$, the disclosure of $\Spec$ restricted to $n$-memory
  schedulers. On the other hand, it is an underapproximation of the
  actual disclosure.


\IncMargin{1em}
\begin{algorithm}
 %\SetAlgoLined
% \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
  \KwIn{ $n \in {\mathbb N}, \Spec, {\cal O}$ and $\varphi$}
  \KwOut{$Disc(n, \Spec, {\cal O}, \varphi)$ such that $Disc_n(\Spec,
    {\cal O}, \varphi) \leq Disc(n, \Spec, {\cal O}, \varphi)\leq
    Disc(\Spec, {\cal O}, \varphi)$}
 
 
 $Disc(n, \Spec, {\cal O}, \varphi) = 0$\;
 \ForEach{$\theta: [n] \times S \rightarrow [n]$ }{ 
 construct $ {\cal A}_{n,\theta} \times \Spec$\; \label{algo:unwind}
 
 compute the set ${\cal M}( {\cal A}_{n,\theta} \times \Spec)$\; 
 
 \tcp{By the procedure described in the proof of Prop.~\ref{thm:MEdetection}} 
 
  \ForEach{$\xi \in 2^{{\cal M}({\cal A}_{n,\theta}, \Spec)}$}{
  construct  $ ({\cal A}_{n,\theta} \times \Spec) \setminus \xi$\; \label{algo:modal}
  
  \If{ $ ({\cal A}_{n,\theta} \times \Spec) \setminus \xi$ is consistent}{compute $Disc(({\cal A}_{n,\theta} \times \Spec) \setminus \xi, {\cal O}, \varphi)$\; \label{algo:disc}
  
  \tcp{By the procedure described in the proof of Thm.~\ref{thm:computNoModal}}
 
  $Disc(n, \Spec, {\cal O}, \varphi) = \max \{{Disc(n, \Spec, {\cal O}, \varphi), Disc(({\cal A}_{n,\theta} \times \Spec) \setminus \xi, {\cal O}, \varphi)}\}$\;
}
  }
 }
 \Return $Disc(n, \Spec, {\cal O}, \varphi)$\;
\caption{Overapproximating $Disc_n(\Spec, {\cal O}, \varphi)$}\label{algomod}
\end{algorithm}\DecMargin{1em}



\paragraph{Correctness.} The partial correctness of
Algorithm~\ref{algomod} relies on Lemma~\ref{lem:rewind} above as well
as the following observation, indicating that the choice of some set
of modal edges to remove exactly corresponds to the scheduler choice:


\begin{lemma}~\label{lem:remove} For any LCMC $\Spec$ and for any
  memoryless scheduler $A = (\A, \gamma)$ of $\Spec$, there exists a
  maximal subset $\xi$ of $\mathcal{M}(\Spec)$ such that for any
  $(s,s') \in \mathcal{M}(\Spec), \gamma(1,s)(s')=0$ iff $(s,s') \in
  \xi$.
     \end{lemma}
      \begin{proof}
        Since the scheduler is memoryless, the choice of scheduling a
        modal edge to $0$ must be uniformly taken along any run, hence
        this edge never appears in the PTS scheduled by $A$.  This is
        equivalent to remove this edge from $\Spec$ so the set $\xi$
        is the collection of all such edges.
\qed
    \end{proof}
     
     \begin{theorem}[Correctness of Algorithm~\ref{algomod}]
       Given a an LCMC $\Spec$, an observation $\cal O$ and a secret
       $\varphi$ for $\Spec$, the disclosure of $\varphi$ in $\Spec$
       for $\cal O$ by a polynomial size adversary (in the size of
       $\Spec$) can be over-approximated in 2EXPTIME.
     \end{theorem}
%      \proof{
%      So,  given an LCMC $\Spec$,
%      from Lemma~\ref{lem:rewind}, we get:
%  $$\sat_n({\cal S}) = \bigcup_{\theta} \sat_1( {\cal A}_{n, \theta} \times \Spec).$$ 
% Hence,  given an observation $\cal O$ and a secret $\varphi$ for $\Spec$, we have:
% $$Disc_n(\Spec, {\cal O}, \varphi) = \sup_{\theta} Disc_1({\cal A}_{n, \theta} \times \Spec, {\cal O}, \varphi).$$
%     From Lemma~\ref{lem:remove} applied to ${\cal A}_{n, \theta} \times \Spec$ for given $n, \theta$, we get:
%      \begin{eqnarray*}
%      \sat_1({\cal A}_{n, \theta} \times \Spec) & = & \bigcup_\xi \sat_1(({\cal A}_{n, \theta} \times \Spec) \setminus \xi)\\
%      & \subseteq & \bigcup_\xi \sat(({\cal A}_{n, \theta} \times \Spec) \setminus \xi).
% %     \\
% %     & \subseteq & \sat({\cal A}_{n, \theta} \times \Spec).
%      \end{eqnarray*}
%     Hence,   given an observation $\cal O$ and a secret $\varphi$ for $\Spec$, we have:
%  % $$Disc_1(({\cal A}_{n, \theta} \times \Spec) \setminus \xi,  {\cal O}, \varphi) \leq Disc(({\cal A}_{n, \theta} \times \Spec) \setminus \xi, {\cal O}, \varphi).$$
%   \begin{eqnarray*}
%   Disc_1(({\cal A}_{n, \theta} \times \Spec), {\cal O}, \varphi)& = &\sup_{\xi} Disc_1(({\cal A}_{n, \theta} \times \Spec) \setminus \xi,  {\cal O}, \varphi) \\
%   & \leq & \sup_{\xi} Disc(({\cal A}_{n, \theta} \times \Spec)\setminus \xi,  {\cal O}, \varphi).
% %  \\
% %  & \leq & Disc(({\cal A}_{n, \theta} \times \Spec), {\cal O}, \varphi).
%    \end{eqnarray*}
%    Putting all together, we obtain the post-condition of algorithm~\ref{algomod}:
%      \begin{eqnarray*}
%    Disc_n(\Spec, {\cal O}, \varphi) & = & \sup_{\theta, \xi}  Disc_1(({\cal A}_{n, \theta} \times \Spec) \setminus \xi,  {\cal O}, \varphi) \\
%    & \leq  & \sup_{\theta, \xi}   Disc(({\cal A}_{n, \theta} \times \Spec)\setminus \xi,  {\cal O}, \varphi)\\
%   & =& Disc(n, \Spec, {\cal O}, \varphi)\\
%   & \leq & Disc(\Spec, {\cal O}, \varphi)
% . 
%     \end{eqnarray*} \JM{Should we write the procedure for computing  $Disc_n$?}

% \paragraph{The complexity } is 2EXPTIME for polynomial size memory schedulers (in the size of $\Spec$).
% }

\begin{proof}
  We prove that the output of Algorithm~\ref{algomod}: $$Disc(n,
  \Spec, \obs, \fee) = \max_\theta \max_\xi Disc( \A_{n,\theta} \times
  \Spec \setminus \xi, \obs, \fee)$$ satisfies the post-condition
  $Disc_n(\Spec, {\cal O}, \varphi) \leq Disc(n, \Spec, {\cal O},
  \varphi)\leq Disc(\Spec, {\cal O}, \varphi)$. For the first
  inequality, let $A=(\A, \gamma)$ be an $n$-memory scheduler for
  $\Spec$ and let $\Spec_\A$ be the corresponding unwinding.  This
  unwinding is computed in the loop of Algorithm~\ref{algomod} at line
  \ref{algo:unwind}. By Lemma~\ref{lem:rewind} the $n$-memory schedulers on $\Spec$  are exactly the memoryless schedulers on $\Spec_\A$.
  Hence the choice function $\gamma$ is memoryless
  on $\A \times \Spec$. Therefore Lemma~\ref{lem:remove} provides a set $\xi$ of
  modal edges to be removed, such that $\Spec_\A$ and $\Spec_\A
  \setminus \xi$ coincide when scheduled by $\gamma$. The removal of
  this set $\xi$ is performed in the inner loop at line
  \ref{algo:modal}. Hence the disclosure of $\Spec(A)$ is less than or
  equal to $Disc(\Spec_\A \setminus \xi, \obs, \fee)$ computed at
  line~\ref{algo:disc} (equality is not ensured since the computation
  of Theorem~\ref{thm:computNoModal} does not restrict memory). This
  value is itself smaller than the output of the algorithm, hence
  $Disc_n(\Spec, {\cal O}, \varphi) \leq Disc(n, \Spec, {\cal O},
  \varphi)$.

  The second inequality results from the fact that all values computed
  by Algorithm~\ref{algomod} are of the form $Disc(\Spec_\A \setminus
  \xi, \obs, \fee)$. Since the maximum is obtained on a subset of
  schedulers, $Disc(n, \Spec, {\cal O}, \varphi)\leq Disc(\Spec, {\cal
    O}, \varphi)$ holds.

  This algorithm makes $2^{O(n)}$ calls to the 2EXPTIME procedure of Theorem~\ref{thm:computNoModal}.
  Thus the complexity of Algorithm~\ref{algomod} remains in 2EXPTIME as long as $n$ is polynomial in the size of $\Spec$.
\qed
\end{proof}