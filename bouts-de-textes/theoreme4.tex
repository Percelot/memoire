\documentclass{article}

%%  Accepte les caractères accentués dans le document (UTF-8).
\usepackage[utf8]{inputenc}
%%
%% Support pour l'anglais et le français (français par défaut).
%\usepackage[cyr]{aeguill}
\usepackage{lmodern}      % Police de caractères plus complète et généralement indistinguable visuellement de la police standard de LaTeX (Computer Modern).
\usepackage[T1]{fontenc}  
\usepackage[frenchb]{babel}
\usepackage{amsmath,amsthm}
\usepackage{amssymb,color}
\newtheorem{mythm}{Théorème}


\newcommand{\om}{\ensuremath{^{\omega}}}
\newcommand{\prob}{\ensuremath{{\bf P}}}
\newcommand{\Spec}{\ensuremath{\mathcal{S}}}
\newcommand{\satO}{\underline{sat}}
\newcommand{\sat}{\mathit{sat}}
\newcommand{\dist}{\ensuremath{\mathcal{D}ist}}
\newcommand{\rel}{\ensuremath{\mathcal{R}}}
\newcommand{\FExec}{\ensuremath{F\!Exec}}
\newcommand{\lst}{\mbox{lst}}

\begin{document}


\begin{mythm}\label{thm:sous-res}
Soient deux spécifications $\Spec_1$ et $\Spec_2$ telles que $\Spec_1$ raffine faiblement $\Spec_2$. Pour tout ordonnanceur $A_1$ de $\Spec_1$, il existe un ordonnanceur $A_2$ de $\Spec_2$ tel que $\Spec_1(A_1)$ raffine $\Spec_2(A_2)$.
\end{mythm}

\begin{proof}
Soient deux spécifications $\Spec_1=(S_1,s_{0,1},T_1,\Sigma,\lambda_1)$ et $\Spec_2=(S_2,s_{0,2},T_2,\Sigma,\lambda_2)$ telles que $\Spec_1$ raffine faiblement $\Spec_2$, et soit un ordonnanceur $A_1\in Sched(\Spec_1)$. Notons $\Spec_1(A_1) = (Q_1,q_{0,1},\Sigma,\Delta_1,L_1)$ le PTS ordonnancé par $A_1$.

L'idée de la preuve consiste à construire le bon ordonnanceur $A_2\in Sched(\Spec_2)$, tel que $\Spec_1(A_1)\rel'\Spec_2(A_2)$.

Notons tout d'abord $\rel$ la relation de raffinement entre $\Spec_1$ et $\Spec_2$.

Soit $\rho_2\in\FExec(\Spec_2)$ avec $\lst(\rho_2)=s_2$. Alors, pour tout $\rho_1\in sim(\rho_2)$, $A_1(\rho_1)\in T_1(\lst(\rho_1))$ et $\lst(\rho_1)\rel s_2$. $\Spec_1$ raffine faiblement $\Spec_2$, donc il existe $\delta_{\rho_1}:S_1\rightarrow \dist(S_2)$ telle que $\sum_{s'_1\in S_1} A_1(\rho_1)\delta_{\rho_1}(s'_1)\in T_2(s_2)$.

On définit $A_2$ tel que :
\[ \forall \rho_2\in \FExec(\Spec_2), A_2(\rho_2) = \sum_{\rho_1\in sim(\rho_2)} \mu_{\rho_2}(\rho_1) \sum_{s'_1\in S_1} A_1(\rho_1)\cdot\delta_{\rho_1}(s'_1)\in T_2(s_2).\]

On en déduit que $A_2$ est bien un ordonnanceur de $\Spec_2$. Montrons maintenant que l'ordonnancement obtenu est raffiné par $\Spec_1(A_1)$ par une relation $\rel'$. 

Dans la suite, on note $\mathcal{A}_1=\Spec_1(A_1)$ et $\mathcal{A}_2=\Spec_2(A_2)$.

Posons $\rel' =\, \sim$ où $\sim$ est la relation de similitude entre deux préfixes finis d'exécutions. Puisque les états de $\mathcal{A}_1$ et de $\mathcal{A}_2$ sont des préfixes finis de leurs spécifications respectives, $\rel'$ est bien une relation entre les états des PTS considérés. Montrons que c'est bien une relation de raffinement.

Soient $\rho_1\in Q_1$ et $\rho_2\in Q_2$ tels que $\rho_1\rel'\rho_2$. 

Posons l'application $\delta':Q_1\rightarrow\dist(Q_2)$ définie par :
\[ 
\delta'(\nu_1)(\nu_2)=
\left\{
\begin{aligned}
\mu_{\rho_2}(\rho_1)\cdot\delta_{\rho_1}(\lst (\nu_1))(\lst (\nu_2)) \;&\; \mbox{si } \nu_1 \rel' \nu_2,\\
0 \;&\; \mbox{sinon.}
\end{aligned}
\right.
\]

Alors, pour tout $\rho'_2\in Q_2$, et en notant $\rho'_2=\rho_2\overset{A_2(\rho_2)}\longrightarrow s'_2$,

\begin{align}
\sum_{\rho'_1\in Q_1} \Delta_1(\rho_1)(\rho'_1)\cdot\delta'(\rho'_1)(\rho'_2) &=& \sum_{s'_1\in S_1} \Delta_1(\rho_1)(\rho_1s'_1)\cdot\delta'(\rho_1s'_1)(\rho'_2)\\
& = & \sum_{s'_1\in S_1} A_1(\rho_1)(s'_1)\cdot\delta'(\rho_1\overset{A_1(\rho_1)}\longrightarrow s'_1)(\rho'_2)\\
& = & \sum_{s'_1\in S_1} A_1(\rho_1)(s'_1)\cdot\mu_{\rho_2}(\rho_1)\cdot\delta_{\rho_1}(s'_1)(s'_2)\\
& = & \sum_{\rho_1\in sim(\rho_2)}\sum_{s'_1\in S_1} A_1(\rho_1)(s'_1)\cdot\mu_{\rho_2}(\rho_1)\cdot\delta_{\rho_1}(s'_1)(s'_2)\\
& = & A_2(\rho_2)(s'_2)\\
& = & \Delta_2(\rho_2)(\rho'_2)
\end{align}

Justifications :
\begin{enumerate}
\item si $\rho'_1$ n'est pas une extension de $\rho_1$, alors $\Delta_1(\rho_1)(\rho'_1) = 0$ car c'est un ordonnanceur, donc la somme sur l'ensemble $Q_1$ est équivalente à la somme sur l'ensemble $S_1$ ;
\item par définition de $\Delta_1$ dans l'ordonnancement ;
\item car $\rho_1\sim\rho_2$ et car $\delta_{\rho_1}(s'_1)(s'_2) > 0$ ssi $s'_1\rel s'_2$ ;
\item {\color{red} car ????} ;
\item par définition de $\Delta_2$ dans l'ordonnancement ;

\end{enumerate}
\end{proof}

\end{document}